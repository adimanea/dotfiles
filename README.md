# My dotfiles

These are the configuration files I use for various programs.

**Note:** They are intended to be used with GNU [`stow`](https://www.gnu.org/software/stow/) and as such, after cloning the repository in the home folder, just `stow` anything you need, **except for `emacs`**, which is not easily supported by `stow`, given its configuration directory structure.

The configurations are not optimized in any way, they are rather minimalistic and show my learning experiments mostly.

OS: Manjaro Linux i3.
