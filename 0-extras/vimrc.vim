scriptencoding utf-8
set encoding=utf-8


" ------ PLUGINS --------- "
set nocompatible                " lose compatibility with simple vi
filetype off                    " required

"" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

"" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'terryma/vim-multiple-cursors'       " multiple cursors plugin
Plugin 'ctrlpvim/ctrlp.vim'                 " fuzzy finder
Plugin 'scrooloose/nerdcommenter'           " toggle comment <Leader>cc, <Leader>cu
Plugin 'scrooloose/nerdtree'                " tree explorer
Plugin 'scrooloose/syntastic'               " syntax checker
Plugin 'farmergreg/vim-lastplace'           " save last place for buffer
Plugin 'lervag/vimtex'                      " lightweight LaTeX suite
Plugin 'ervandew/supertab'                  " tab completion
"Plugin 'Yggdroot/indentLine'                " show indent levels (|)
"Plugin 'ryanoasis/vim-devicons'             " icons for NerdTree etc.
"Plugin 'tpope/vim-fugitive'                 " git integration
Plugin 'sheerun/vim-polyglot'               " better syntax highlighting
Plugin 'dhruvasagar/vim-table-mode'         " for table creation

" COLORSCHEMES
Plugin 'altercation/vim-colors-solarized'   " solarized color scheme
Plugin 'dracula/vim'                        " dracula colorscheme
Plugin 'arcticicestudio/nord-vim'           " nord colorscheme
Plugin 'morhetz/gruvbox'                    " gruvbox colorscheme
Plugin 'chriskempson/base16-vim'            " base16 colorschemes
Plugin 'whatyouhide/vim-gotham'             " gotham color scheme
Plugin 'joshdick/onedark.vim'               " atom one dark colorscheme
Plugin 'zacanger/angr.vim'                 " angr colorscheme

" Airline
"Plugin 'vim-airline/vim-airline'
"Plugin 'vim-airline/vim-airline-themes'
"Plugin 'enricobacis/vim-airline-clock'      " datetime in airline


"" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" Put your non-Plugin stuff after this line


""" BASIC TOOLS
" Navigating with guides (using <++> placeholder)
inoremap <C-j> <esc>/<++><enter>"_c4l
vnoremap <C-j> <Esc>/<++><Enter>"_c4l
map <C-j> <Esc>/<++><Enter>"_c4l

"set numberwidth=6           " width for the line number column
"set columns                " soft wrap at 80 columns
set clipboard=unnamed       " interact with system clipboard
set ttimeout                " timeout for key codes
set ttimeoutlen=10          " 10ms
set timeout                 " for mappings
set timeoutlen=1000         " 1 second, default
set ttyfast                 " faster redrawing
set backspace=indent,eol,start		" use backspace as normal
let mapleader = ','			" the <Leader> key for prefixes
set scrolloff=3             " keep 3 lines below and above the cursor
set undolevels=25           " maximum allowed
set nonumber                  " show line numbers
"set relativenumber          " relative line numbers
set incsearch				" incremental search
"set hlsearch                " highlight search results
nmap <Leader><Space> :nohlsearch
set guioptions-=l			" eliminate scrollbars
set guioptions-=L
set guioptions-=r
set guioptions-=R
set splitbelow				" default h split puts below
set splitright				" default v split puts to the right
set noerrorbells visualbell t_vb=	" disable bell
set autowriteall			" autosave when switching buffers
set shiftwidth=4            " 4 spaces in (auto)indent
set tabstop=4				" tab in normal mode
set softtabstop=4			" tab in insert mode
set expandtab				" convert tabs to spaces
"set foldlevelstart=10       " unfold most folds at start
"set foldnestmax=10          " 10 nested folds max
"set foldmethod=indent       " folds based on indent level
set autoread                " reload files changed outside of vim
set wrapmargin=0
set history=100             " how many lines of history it remembers
set showmatch               " highlight matching brackets when over them
set ai                      " auto indent
set si                      " smart indent
set linebreak    "Wrap lines at convenient points
" ignore some FileTypes for completion in the Command area
set wildignore+=*.aux,*.toc,*.synctex.gz,*.fdb_latexmk
set wildmenu                " visual autocomplete for cmd menu
"set modelines=1             " allow settings for current file only

" show whitespace at the end of a line
highlight WhitespaceEOL ctermbg=blue ctermfg=blue guibg=blue
match WhitespaceEOL /\s\+$/


" Show syntax highlighting groups for word under cursor
nmap <Leader>sy :call <SID>SynStack()<CR>
function! <SID>SynStack()
    if !exists("*synstack")
        return
    endif
    echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc

" show syntax color for element under cursor
nmap <Leader>sc :echo synIDattr(synIDtrans(synID(line("."), col("."), 1)), "fg")<CR>


"" airline config:
"" always show statusline
"set laststatus=2
"" start with a blank template
"set statusline=
"" choose airline extensions
"let g:airline_extensions = ['clock']
""let g:airline_powerline_fonts = 1              " for file icons
"" shorthand mode notation for modes
"let g:airline_mode_map = {
            "\ '__' : '-',
            "\ 'n'  : 'N',
            "\ 'i'  : 'I',
            "\ 'R'  : 'R',
            "\ 'c'  : 'C',
            "\ 'v'  : 'V',
            "\ 'V'  : 'V',
            "\ '' : 'V',
            "\ 's'  : 'S',
            "\ 'S'  : 'S',
            "\ '' : 'S',
            "\ }
"let g:airline#extensions#tabline#enabled = 1
"let g:airline#extensions#clock#format = '[%a,%e %b %H:%M]'

"" simplify some stuff displayed
"if !exists('g:airline_symbols')
    "let g:airline_symbols = {}
"endif
"let g:airline_symbols.linenr = ''
"let g:airline_symbols.maxlinenr = ''

"" disable detection of whitespace errors
"silent! call airline#extensions#whitespace#disable()
"let g:airline_theme='minimalist'


" indentLine config
" vertical line indentation
"let g:indentLine_color_term = 100
"let g:indentLine_color_gui = '#878700'  "yellow
"let g:indentLine_char = '|'
" see colors here
" http://vim.wikia.com/wiki/Xterm256_color_names_for_console_Vim


" ruler with date and time
" show statusline
set laststatus=2
set ruler
set rulerformat=%33(%{strftime('%a\ %e\ %b\ %H:%M')}\ %5l,%-6(%c%)\ %P%)

" show trailing chars
"set list
set listchars=tab:>-,eol:-,trail:.,nbsp:~,extends:>,precedes:<,space:*


" Set the command window height to 2 lines, to avoid many cases of having to
" press <Enter> to continue"
set cmdheight=2

"Searching
set ignorecase              " case insensitive searching
set smartcase               " case sensitive if expression contains a capital
set magic                   " set magic on, for regex


" set Vim-specific sequences for RGB colors
" fix for working inside tmux !
"let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
"let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

syntax on
set background=dark
colorscheme my-1
"hi texMathZoneX ctermfg=2
"hi texMathMatcher ctermfg=2
"hi texMathZoneW ctermfg=2
"hi texMathZoneAS ctermfg=2
"hi Delimiter ctermfg=4 guifg=#8FBCBB
"set colorcolumn=79
" display colorcolumn at 79 columns

"-----MY THEMING------"
"hi Normal ctermbg=238 ctermfg=Yellow guifg=Yellow guibg=grey20
"" Tab bar
"hi TabLine ctermbg=None ctermfg=Black guibg=black guifg=LightGray
"hi TabLineFill cterm=underline
""hi TabLineSel cterm=underline ctermfg=Yellow guifg=Yellow guibg=grey20
"hi ColorColumn cterm=underline ctermbg=DarkGray gui=underline guibg=grey20
"hi StatusLine cterm=underline ctermfg=LightGreen gui=underline guifg=burlywood1
"hi StatusLineNC term=underline cterm=underline gui=underline
"hi EndOfBuffer ctermbg=238 ctermfg=DarkGreen guibg=black guifg=DarkGreen
"hi VertSplit ctermbg=100
"hi Comment ctermfg=100 guifg=LightGray
"hi texComment ctermbg=100 guibg=LightGray

" -- SETTINGS FOR BASE16 --
"let base16colorspace=256
"colorscheme base16-eighties

"make the GUI version use the same colors as term
"if has("termguicolors")
"set termguicolors
"endif

" -- SETTINGS FOR SOLARIZED --
"let g:solarized_visibility = "high"
"let g:solarized_contrast = "high"
"let g:solarized_termcolors = 16
"let g:solarized_termtrans = 1
"colorscheme solarized


" ---------GUI Options---------"
if has("gui_macvim")
    set linespace=7
    "set guifont=PragmataPro:h13
    set guifont=Consolas\ for\ BBEdit:h13
    set antialias " noantialias if not
    colorscheme onedark
    "set guifont=Input\ Mono\ Narrow:h12
    "set guifont=DejaVu\ Sans\ Mono:h11
    "set guifont=Terminus\ (TTF):h15
    set macligatures
    "let g:gruvbox_contrast_dark="hard"
    "colorscheme gruvbox
    "let g:solarized_visibility = "high"
    "let g:solarized_contrast = "normal"
    "let g:solarized_termcolors = 16
    "let g:solarized_termtrans = 1
    "colorscheme solarized
    " --- MY THEMING ---
    "hi Normal guifg=Yellow guibg=grey20
    "Tab bar
    "hi ColorColumn gui=underline guibg=grey20
    "hi StatusLine gui=underline guifg=burlywood1
    "hi StatusLineNC gui=underline
    "hi EndOfBuffer guibg=black guifg=DarkGreen
    "hi VertSplit guibg=LightGray
    "hi Comment guifg=LightGray
    "hi texComment guibg=LightGray
endif

" Scroll other window with M-j, M-k
nmap <a-j> <c-w>w<c-e><c-w>w
nmap <a-k> <c-w>w<c-y><c-w>w

" This makes vim act like all other editors, buffers can
" exist in the background without being in a window.
" http://items.sjbach.com/319/configuring-vim-right
set hidden


" ================ Persistent Undo ==================
" Keep undo history across sessions, by storing in file.
" Only works all the time.
if has('persistent_undo') && isdirectory(expand('~').'/.vim/backups')
    silent !mkdir ~/.vim/backups > /dev/null 2>&1
    set undodir=~/.vim/backups
    set undofile
endif


" ---------General-LaTeX-----------"
filetype indent on
set grepprg=grep\ -nH\ $*       " better search in tex source files
let g:tex_flavor='latex'        " set default type for .tex
let g:Tex_DefaultTargetFormat='pdf'
let g:tex_fast='c,m,M,p,r,s,S,v,V'
let g:tex_conceal=""            " do not hide $ and other symbols
set matchpairs+={:}

" Setup the compile rule for pdf to use pdflatex with synctex enabled
let g:Tex_CompileRule_pdf = 'pdflatex -synctex=1 --interaction=nonstopmode $*' 
" PDF display rule
let g:Tex_ViewRule_pdf = 'open -a Skim'

" ---------Markdown---------------"
"  fix for conceal (due to indentLine)
autocmd FileType markdown let g:indentLine_enabled=0
" soft wrap for Markdown
"autocmd FileType markdown set columns=80
" also for txt
"autocmd FileType text set columns=80


" --------vimtex-----------------"
nmap <leader>xt :VimtexTocToggle<CR>
let g:vimtex_compiler_latexmk = {'callback' : 0}
"let g:vimtex_matchparen_enabled=0


"----------NERDTree-----------------"
nmap <leader>nt :NERDTreeToggle<CR> 


"--------SYNTASTIC----------------"
" allow navigation between errors with :lnext :lprev
let g:syntastic_always_populate_loc_list = 1


" ----------Spelling---------- "
" zg learn word
" zG learn this time only
" [s ]s goes between spellings
" z= gets suggestions
" C-X C-K suggests IN INSERT MODE
set spelllang=en
set spellfile=$HOME/.vim/spelling/.vim-spell-en.utf-8.add

map <leader>sr :set spelllang=roc<cr>
map <leader>se :set spelllang=en<cr>
map <leader>ss :setlocal spell!<cr>

" ------Mappings----- "
" move vertically by visual line (in case of wrap)
nnoremap j gj
nnoremap k gk
map î :tabnext<CR>
map ă :tabprevious<CR>

" disable highlight of search res
nmap <Leader><space> :nohlsearch<cr>

" edit vimrc in a new tab
nmap <Leader>ev :tabedit ~/.vimrc<cr>

" source current file
nmap <Leader>so :source %<cr>

"---------CtrlP-------------"
let g:ctrlp_match_window = 'bottom,order:ttb'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 0
let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'


"-----------BACKUP---------------"
set backup
set backupdir=~/.vim-tmp
" for swap
set directory=~/.vim-tmp

"------------Multiple Cursors---------"
" Default mapping
let g:multi_cursor_next_key='<C-n>'
let g:multi_cursor_prev_key='<C-p>'
let g:multi_cursor_skip_key='<C-x>'
let g:multi_cursor_quit_key='<Esc>'



"--------LaTeX---------"
" Text styles
autocmd BufNewFile,BufRead *.tex inoremap ;em \emph{}<++><Esc>T{i
autocmd BufNewFile,BufRead *.tex inoremap ;bf \textbf{}<++><Esc>T{i
autocmd BufNewFile,BufRead *.tex inoremap ;it \textit{}<++><Esc>T{i
autocmd BufNewFile,BufRead *.tex inoremap ;tt \texttt{}<++><Esc>T{i
autocmd BufNewFile,BufRead *.tex inoremap ;sc \textsc{}<++><Esc>T{i
autocmd BufNewFile,BufRead *.tex inoremap ;fk \mathfrak{}<++><Esc>T{i
autocmd BufNewFile,BufRead *.tex inoremap ;bb \mathbb{}<++><Esc>T{i
autocmd BufNewFile,BufRead *.tex inoremap ;ka \mathcal{}<++><Esc>T{i

" Environments
autocmd BufNewFile,BufRead *.tex inoremap ;ol \begin{enumerate}<Enter><Enter>\end{enumerate}<Enter><Enter><++><Esc>3kA\item<Space>
autocmd BufNewFile,BufRead *.tex inoremap ;aol \begin{enumerate}[(a)]<Enter><Enter>\end{enumerate}<Enter><Enter><++><Esc>3kA\item<Space>
autocmd BufNewFile,BufRead *.tex inoremap ;1ol \begin{enumerate}[(1)]<Enter><Enter>\end{enumerate}<Enter><Enter><++><Esc>3kA\item<Space>
autocmd BufNewFile,BufRead *.tex inoremap ;ul \begin{itemize}<Enter><Enter>\end{itemize}<Enter><Enter><++><Esc>3kA\item<Space>
autocmd BufNewFile,BufRead *.tex inoremap ;li \item<Space>
autocmd BufNewFile,BufRead *.tex inoremap ;tab \begin{tabular}<Enter><++><Enter>\end{tabular}<Enter><Enter><++><Esc>4kA{}<Esc>i
autocmd BufNewFile,BufRead *.tex inoremap ;sec \section{}<Enter><Enter><++><Esc>2kf}i
autocmd BufNewFile,BufRead *.tex inoremap ;ssec \subsection{}<Enter><Enter><++><Esc>2kf}i
autocmd BufNewFile,BufRead *.tex inoremap ;sssec \subsubsection{}<Enter><Enter><++><Esc>2kf}i
autocmd BufNewFile,BufRead *.tex inoremap ;beg \begin{%DELRN%}<Enter><++><Enter>\end{%DELRN%}<Enter><Enter><++><Esc>4kfR:MultipleCursorsFind<Space>%DELRN%<Enter>c
autocmd BufNewFile,BufRead *.tex inoremap ;up <Esc>/usepackage<Enter>o\usepackage{}<Esc>i
autocmd BufNewFile,BufRead *.tex nnoremap ;up /usepackage<Enter>o\usepackage{}<Esc>i
autocmd BufNewFile,BufRead *.tex inoremap ;al* \begin{align*}<Enter><Space><Space><Enter>\end{align*}<Enter><Enter><++><Esc>3kA

"""" Math Environments
autocmd BufNewFile,BufRead *.tex inoremap ;thm \begin{theorem}\label{thm:}<Enter><++><Enter>\end{theorem}<Enter><Enter><++><Esc>4k$i
autocmd BufNewFile,BufRead *.tex inoremap ;nth \begin{theorem}[]\label{thm:<++>}<Enter><++><Enter>\end{theorem}<Enter><Enter><++><Esc>4kf[a
autocmd BufNewFile,BufRead *.tex inoremap ;pp \begin{proposition}\label{prop:}<Enter><++><Enter>\end{proposition}<Enter><Enter><++><Esc>4k$i
autocmd BufNewFile,BufRead *.tex inoremap ;npp \begin{proposition}[]\label{prop:}<Enter><++><Enter>\end{proposition}<Enter><Enter><++><Esc>4kf[a
autocmd BufNewFile,BufRead *.tex inoremap ;le \begin{lemma}\label{le:}<Enter><++><Enter>\end{lemma}<Enter><Enter><++><Esc>4k$i
autocmd BufNewFile,BufRead *.tex inoremap ;nle \begin{lemma}[]\label{le:<++>}<Enter><++><Enter>\end{lemma}<Enter><Enter><++><Esc>4kf[a
autocmd BufNewFile,BufRead *.tex inoremap ;co \begin{corollary}\label{cor:}<Enter><++><Enter>\end{corollary}<Enter><Enter><++><Esc>4k$i
autocmd BufNewFile,BufRead *.tex inoremap ;nco \begin{corollary}[]\label{cor:}<Enter><++><Enter>\end{corollary}<Enter><Enter><++><Esc>4kf[a
autocmd BufNewFile,BufRead *.tex inoremap ;pf \begin{proof}<Enter><Enter>\end{proof}<Enter><Enter><++><Esc>3k$i
autocmd BufNewFile,BufRead *.tex inoremap ;def \begin{definition}\label{def:}<Enter><++><Enter>\end{definition}<Enter><Enter><++><Esc>4k$i
autocmd BufNewFile,BufRead *.tex inoremap ;ex \begin{example}<Enter><Enter>\end{example}<Enter><Enter><++><Esc>3k$i
autocmd BufNewFile,BufRead *.tex inoremap ;rk \begin{remark}\label{rk:}<Enter><++><Enter>\end{remark}<Enter><Enter><++><Esc>4k$i
autocmd BufNewFile,BufRead *.tex inoremap ;cm \[<Enter><Space><Space><Enter>\]<Enter><++><Esc>2k$a
autocmd BufNewFile,BufRead *.tex inoremap ;ca \begin{cases}<Enter><Space><Space><Space>&<Space><++>\\<Enter><++><Enter>\end{cases}<Enter><++><Esc>3ki




""" Symbols and Commands 
autocmd BufNewFile,BufRead *.tex inoremap ;ref \ref{}<Space><++><Esc>T{i
autocmd BufNewFile,BufRead *.tex inoremap ;ra \rightarrow
autocmd BufNewFile,BufRead *.tex inoremap ;la \leftarrow
autocmd BufNewFile,BufRead *.tex inoremap ;lra \leftrightarrow
autocmd BufNewFile,BufRead *.tex inoremap ;fa \forall
autocmd BufNewFile,BufRead *.tex inoremap ;ex \exists
autocmd BufNewFile,BufRead *.tex inoremap ;Ra \Rightarrow
autocmd BufNewFile,BufRead *.tex inoremap ;La \Leftarrow
autocmd BufNewFile,BufRead *.tex inoremap ;Lra \Leftrightarrow
autocmd BufNewFile,BufRead *.tex inoremap ;LRa \Longrightarrow
autocmd BufNewFile,BufRead *.tex inoremap ;LLR \Longleftrightarrow
autocmd BufNewFile,BufRead *.tex inoremap ;pd \frac{\partial }{\partial <++>}<Space><++><Esc>2F}i
autocmd BufNewFile,BufRead *.tex inoremap ;00 \infty
autocmd BufNewFile,BufRead *.tex inoremap ;\ \textbackslash
autocmd BufNewFile,BufRead *.tex inoremap ;iii \int_{-\infty}^{\infty} 
autocmd BufNewFile,BufRead *.tex inoremap ;ioi \int_0^\infty 




""" Greek Letters
autocmd BufNewFile,BufRead *.tex inoremap ,a \alpha
autocmd BufNewFile,BufRead *.tex inoremap ,b \beta
autocmd BufNewFile,BufRead *.tex inoremap ,e \varepsilon
autocmd BufNewFile,BufRead *.tex inoremap ,f \varphi
autocmd BufNewFile,BufRead *.tex inoremap ,l \lambda
autocmd BufNewFile,BufRead *.tex inoremap ,L \Lambda
autocmd BufNewFile,BufRead *.tex inoremap ,g \gamma
autocmd BufNewFile,BufRead *.tex inoremap ,G \Gamma
autocmd BufNewFile,BufRead *.tex inoremap ,d \delta
autocmd BufNewFile,BufRead *.tex inoremap ,D \Delta
autocmd BufNewFile,BufRead *.tex inoremap ,t \theta
autocmd BufNewFile,BufRead *.tex inoremap ,T \Theta
autocmd BufNewFile,BufRead *.tex inoremap ,o \omega
autocmd BufNewFile,BufRead *.tex inoremap ,O \Omega

" forceful fix for thehighlight in emph
"autocmd BufNewFile,BufRead *.tex :colorscheme my-simple

" no soft wrap in latex
"autocmd BufNewFile,BufRead *.tex :set columns=<CR>




""" Most common misspellings
iab đfrac dfrac
iab đots dots
iab ßqrt sqrt
iab ƒrac frac
iab ƒorall forall
