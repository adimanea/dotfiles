#+TITLE: Spare Packages
#+AUTHOR: Adrian


* Visual Fill Column
Soft wraps at a specific column number.
Example config:
#+BEGIN_SRC elisp
  (use-package visual-fill-column
    :config
    (global-visual-fill-column-mode -1))
  (setq-default fill-column 0) ;; let it decide on its own the wrap
#+END_SRC
[[https://github.com/joostkremers/visual-fill-column][GitHub Source]].

* Prolog Support (Ediprolog & ~swipl~)
#+BEGIN_SRC elisp
  ;; === PROLOG
  (load "/home/t3rtius/.emacs.d/mine/3-packages/prolog/prolog.el")

  (use-package ediprolog
    :defer t)
    :config
    (setq ediprolog-program "/bin/swipl"))
  (global-set-key [f10] 'ediprolog-dwim)
  (autoload 'run-prolog "prolog" "Start a Prolog subprocess." t)
  (autoload 'prolog-mode "prolog" "Major mode for editing Prolog programs." t)
  (autoload 'mercury-mode "prolog" "Major mode for editing Mercury programs." t)
  (setq prolog-system 'swi)
  (setq prolog-program-name "/bin/swipl") ;; for inferior-prolog
  (setq auto-mode-alist (append '(("\\.pl$" . prolog-mode)
                                  ("\\.m$" . mercury-mode))
                                auto-mode-alist))

  ;; === FLYCHECK for PROLOG
  (add-hook 'prolog-mode-hook
            (lambda ()
              (require 'flymake)
              (make-local-variable 'flymake-allowed-file-name-masks)
              (make-local-variable 'flymake-err-line-patterns)
              (setq flymake-err-line-patterns
                    '(("ERROR: (?\\(.*?\\):\\([0-9]+\\)" 1 2)
                      ("Warning: (\\(.*\\):\\([0-9]+\\)" 1 2)))
              (setq flymake-allowed-file-name-masks
                    '(("\\.pl\\'" flymake-prolog-init)))
              (flymake-mode 1)))

  (defun flymake-prolog-init ()
    (let* ((temp-file   (flymake-init-create-temp-buffer-copy
                         'flymake-create-temp-inplace))
           (local-file  (file-relative-name
                         temp-file
                         (file-name-directory buffer-file-name))))
      (list "swipl" (list "-q" "-t" "halt" "-s " local-file))))


  ;; === PROLOG ORG-BABEL SUPPORT
  (use-package ob-prolog
    :defer t
    :config
    (setq prolog-program-name 'swipl)
    (org-babel-do-load-languages
     'org-babel-load-languages
     '((prolog . t))))
#+END_SRC

* undo-tree
Is it now built-in? *NO*!
#+BEGIN_SRC elisp
  (use-package undo-tree
    :diminish undo-tree-mode
    :config
    (global-undo-tree-mode)
    (setq undo-tree-visualizer-timestamps t)
    (setq undo-tree-visualizer-diff t))
#+END_SRC
* recentf
Is it now built-in?
#+BEGIN_SRC elisp
  (use-package recentf
    :config
    (recentf-mode)
    (setq recentf-max-saved-items 200
          recentf-max-menu-items 15))
#+END_SRC

* Flyspell
Is it abandoned?
#+BEGIN_SRC elisp
  (use-package flyspell
    :diminish flyspell-mode
    :config
    (setq ispell-program-name "aspell")
    (setq ispell-alternate-dictionary nil)
    (setq ispell-dictionary "english")
    (setq ispell-local-dictionary nil)
    (defun fd-switch-dictionary()
      "Switch between English and Romanian dicts"
      (interactive)
      (let* ((dic ispell-current-dictionary)
             (change (if (string= dic "english") "ro-classic" "english")))
        (ispell-change-dictionary change)
        (message "Dictionary switched from %s to %s" dic change)
        ))
    (defun my-save-word ()
      "Add word to dictionary (Learn)"
      (interactive)
      (let ((current-location (point))
            (word (flyspell-get-word)))
        (when (consp word)    
          (flyspell-do-correct 'save nil
                               (car word) current-location (cadr word)
                               (caddr word) current-location))))
    (global-set-key (kbd "C-c fly") 'flyspell-mode)
    (define-key flyspell-mode-map (kbd "C-c fs") 'my-save-word)
    (define-key flyspell-mode-map (kbd "C-c er") 'fd-switch-dictionary)
    (define-key flyspell-mode-map (kbd "C-M-i") nil)
    (define-key flyspell-mode-map (kbd "M-TAB") nil)
    (define-key flyspell-mode-map (kbd "C-,") nil)
    (define-key flyspell-mode-map (kbd "C-,") 'flyspell-auto-correct-previous-word))
#+END_SRC
* Custom Functions
** Copy filename or path in dired
#+BEGIN_SRC elisp
  ;; === DIRED COPY FILENAME OR PATH
  (defun copy-buffer-file-name-as-kill (choice)
    "Copy the buffer-file-name to the kill-ring"
    (interactive "cCopy Buffer Name (F) Full, (D) Directory, (N) Name")
    (let ((new-kill-string)
          (name (if (eq major-mode 'dired-mode)
                    (dired-get-filename)
                  (or (buffer-file-name) ""))))
      (cond ((eq choice ?f)
             (setq new-kill-string name))
            ((eq choice ?d)
             (setq new-kill-string (file-name-directory name)))
            ((eq choice ?n)
             (setq new-kill-string (file-name-nondirectory name)))
            (t (message "Quit")))
      (when new-kill-string
        (message "%s copied" new-kill-string)
        (kill-new new-kill-string))))
#+END_SRC

** Hide Modeline
#+BEGIN_SRC elisp
  ;; Sometimes we don't want a mode-line. Use M-x hidden-mode-line to activate.
  (defvar-local hidden-mode-line-mode nil)
  (define-minor-mode hidden-mode-line-mode
    "Minor mode to hide the mode-line in the current buffer."
    :init-value nil
    :global t
    :variable hidden-mode-line-mode
    :group 'editing-basics
    (if hidden-mode-line-mode
        (setq hide-mode-line mode-line-format
              mode-line-format nil)
      (setq mode-line-format hide-mode-line
            hide-mode-line nil))
    (force-mode-line-update)
    ;; Apparently force-mode-line-update is not always enough to
    ;; redisplay the mode-line
    (redraw-display)
    (when (and (called-interactively-p 'interactive)
               hidden-mode-line-mode)
      (run-with-idle-timer
       0 nil 'message
       (concat "Hidden Mode Line Mode enabled.  "
               "Use M-x hidden-mode-line-mode to make the mode-line appear."))))
#+END_SRC

** Swap windows in a split
#+BEGIN_SRC elisp
  ;; === Swap windows in split
  (defun win-swap ()
    "Swap windows using buffer-move.el" ;; it seems it's built-in
    (interactive)
    (if
        (null (windmove-find-other-window 'right))
        (buf-move-left)
      (buf-move-right)))
#+END_SRC

** PDF Scroll Other Window Bug (macOS only?)
#+BEGIN_SRC elisp
  ;; ===FIX FOR PDF-VIEW SCROLL-OTHER-WINDOW
  (defvar-local sow-scroll-up-command nil)

  (defvar-local sow-scroll-down-command nil)

  (defvar sow-mode-map
    (let ((km (make-sparse-keymap)))
      (define-key km [remap scroll-other-window] 'sow-scroll-other-window)
      (define-key km [remap scroll-other-window-down] 'sow-scroll-other-window-down)
      km)
    "Keymap used for `sow-mode'")

  (define-minor-mode sow-mode
    "FIXME: Not documented."
    nil nil nil
    :global t)

  (defun sow-scroll-other-window (&optional arg)
    (interactive "P")
    (sow--scroll-other-window-1 arg))

  (defun sow-scroll-other-window-down (&optional arg)
    (interactive "P")
    (sow--scroll-other-window-1 arg t))

  (defun sow--scroll-other-window-1 (n &optional down-p)
    (let* ((win (other-window-for-scrolling))
           (cmd (with-current-buffer (window-buffer win)
          (if down-p
              (or sow-scroll-down-command #'scroll-up-command)
            (or sow-scroll-up-command #'scroll-down-command)))))
      (with-current-buffer (window-buffer win)
        (save-excursion
          (goto-char (window-point win))
          (with-selected-window win
            (funcall cmd n))
          (set-window-point win (point))))))

  (add-hook 'Info-mode-hook
        (lambda nil
          (setq sow-scroll-up-command
            (lambda (_) (Info-scroll-up))
            sow-scroll-down-command
            (lambda (_) (Info-scroll-down)))))

  (add-hook 'doc-view-mode-hook
            (lambda nil
              (setq sow-scroll-up-command
                    'doc-view-scroll-up-or-next-page
                    sow-scroll-down-command
                    'doc-view-scroll-down-or-previous-page)))

  (add-hook 'pdf-view-mode-hook
            (lambda nil
              (setq sow-scroll-up-command
                    'pdf-view-scroll-up-or-next-page
                    sow-scroll-down-command
                    'pdf-view-scroll-down-or-previous-page)))

  (provide 'scroll-other-window)
  ;;; scroll-other-window.el ends here


  (global-unset-key (kbd "C-M-v")) ;; original scroll other window DOWN
  (global-unset-key (kbd "M-<prior>"))
  (global-unset-key (kbd "C-M-S-v")) ;; original scroll other window UP
  (global-unset-key (kbd "M-<next>"))
  (define-key global-map (kbd "M-<prior>") 'sow-scroll-other-window-down)
  (define-key global-map (kbd "M-<next>") 'sow-scroll-other-window)
#+END_SRC

** Org open PDF at point (with ~pdfview~)
#+BEGIN_SRC elisp
  ;; === ORG OPEN AT POINT FOR PDF
  (defun my-org-open-at-point()
    (interactive)
    (let ((org-file-apps '(("\\.pdf\\'" . "pdfview %s"))))
      (org-open-at-point)
      ))
#+END_SRC
* Ivy
#+BEGIN_SRC elisp
  ;; === IVY
  ;; Documentation: https://oremacs.com/swiper/#key-bindings
  (use-package ivy
    :init
    (ido-mode -1)
    :diminish ivy-mode
    :config
    (ivy-mode 1)
    (setq ivy-use-virtual-buffers t)
    (setq enable-recursive-minibuffers t)
    (global-set-key (kbd "C-x b") 'ivy-switch-buffer))
#+END_SRC
* My Custom Faces
#+BEGIN_SRC elisp
  ;; === MY CUSTOM FACES
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; Use M-x RET list-faces-display RET to view all available faces
  ;; Note that activating a major mode could possibly add new faces to the list

  ;; Use M-x RET describe-face RET to view what a face is used for

  ;; The below covers pretty much all the faces that annoy me in some themes.
  ;; Use them in an 'à la carte' fashion, i.e. comment/uncomment whatever you need,
  ;; depending on the theme and desired result.
  ;; What I invariably hate is italic font for comments, so that'll be always on.


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; === GLOBAL FACES
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; === MAKE ITALIC NORMAL
  ;; (set-face-attribute 'italic nil
  ;;                     :slant 'normal
  ;;                     :weight 'normal
  ;;                     :foreground nil ;; is this necessary?
  ;;                     :inherit 'font-lock-string-face) ;; to have it colored
  ;; ;; === MAKE BOLD NORMAL
  ;; (set-face-attribute 'bold nil
  ;;                     :weight 'normal
  ;;                     :foreground nil
  ;;                     :inherit 'font-lock-string-face) ;; to have it colored
  ;; ;; === MAKE BOLD ITALIC NORMAL
  ;; (set-face-attribute 'bold-italic nil
  ;;                     :slant 'normal
  ;;                     :weight 'bold
  ;;                     :foreground nil
  ;;                     :inherit 'font-lock-string-face) ;; to have it colored
  ;; ;; === FIXED PITCH
  ;; (set-face-attribute 'fixed-pitch nil
  ;;                     :height 1.2) ;; Courier is small
  ;; (set-face-attribute 'fixed-pitch-serif nil
  ;;                     :inherit 'fixed-pitch)
  ;; ;; === SUCCESS
  ;; (set-face-attribute 'success nil
  ;;                     :weight 'normal)
  ;; ;; === ERROR
  ;; (set-face-attribute 'error nil
  ;;                     :weight 'normal)
  ;; ;; === ISEARCH
  ;; (set-face-attribute 'isearch-fail nil
  ;;                     :weight 'normal)
  ;; (set-face-attribute 'isearch nil
  ;;                     :weight 'normal)
  ;; ;; === PACKAGE NAME & STATUS
  ;; (set-face-attribute 'package-name nil
  ;;                     :weight 'normal)
  ;; (set-face-attribute 'package-status-disabled nil
  ;;                     :weight 'normal)
  ;; ;; === ALWAYS-ON MATCHES (e.g. in ivy menu)
  ;; (set-face-attribute 'match nil
  ;;                     :weight 'normal)
  ;; ;; === INFO
  ;; (set-face-attribute 'info-header-node nil
  ;;                     :weight 'normal)
  ;; (set-face-attribute 'info-node nil
  ;;                     :weight 'normal)


  ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; ;; === VARIABLE WIDTH (DISABLE)
  ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (set-face-attribute 'variable-pitch nil
                      :family "Monospace"
                      :inherit 'default)
  (set-face-attribute 'info-menu-header nil
                      :inherit 'default
                      :weight 'normal)
  (set-face-attribute 'info-title-1 nil
                      :inherit 'default
                      :weight 'normal)
  (set-face-attribute 'info-title-2 nil
                      :inherit 'default
                      :weight 'normal)
  (set-face-attribute 'info-title-3 nil
                      :inherit 'default
                      :weight 'normal)
  (set-face-attribute 'info-title-4 nil
                      :inherit 'default
                      :weight 'normal)


  ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; ;; === MODELINE & MINIBUFFER
  ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; (set-face-attribute 'minibuffer-prompt nil
  ;;                     :weight 'normal)
  ;; ;; === MODELINE BUFFER ID
  ;; (set-face-attribute 'mode-line-buffer-id nil
  ;;                     :weight 'normal)
  ;; ;; === MODELINE EMPHASIS
  ;; (set-face-attribute 'mode-line-emphasis nil
  ;;                     :weight 'normal
  ;;                     :inherit 'font-lock-keyword-face) ;; use some color
  ;; ;; === BUFFER NAMES IN THE BUFFER MENU
  ;; (set-face-attribute 'buffer-menu-buffer nil
  ;;                     :weight 'normal)


  ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; ;; === FONT-LOCK FACES
  ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; ;; === BUILTINS
  ;; (set-face-attribute 'font-lock-builtin-face nil
  ;;                     :weight 'normal
  ;;                     :foreground nil
  ;;                     :inherit 'font-lock-keyword-face) ;; use the same as for keywords
  ;; ;; === CONSTANTS AND LABELS
  ;; (set-face-attribute 'font-lock-constant-face nil
  ;;                     :weight 'normal)
  ;; ;; === KEYWORDS
  ;; (set-face-attribute 'font-lock-keyword-face nil
  ;;                     :weight 'normal)
  ;; ;; === NEGATION
  ;; (set-face-attribute 'font-lock-negation-char-face nil
  ;;                     :weight 'normal)
  ;; ;; === REGEX
  ;; (set-face-attribute 'font-lock-regexp-grouping-backslash nil
  ;;                     :weight 'normal)
  ;; (set-face-attribute 'font-lock-regexp-grouping-construct nil
  ;;                     :weight 'normal)
  ;; ;; === WARNING
  ;; (set-face-attribute 'font-lock-warning-face nil
  ;;                     :weight 'normal)
  ;; ;; === FUNCTION NAME
  ;; (set-face-attribute 'font-lock-function-name-face nil
  ;;                     :weight 'normal)
  ;; === COMMENT & DELIMITER
  (set-face-attribute 'font-lock-comment-face nil
                      :slant 'normal)
  (set-face-attribute 'font-lock-comment-delimiter-face nil
                      :inherit 'font-lock-comment-face
                      :slant 'normal)



  ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; ;; === DIRED FACES
  ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; (set-face-attribute 'dired-mark nil
  ;;                     :foreground nil
  ;;                     :weight 'normal
  ;;                     :inherit 'success)
  ;; (set-face-attribute 'dired-marked nil
  ;;                     :foreground nil
  ;;                     :weight 'normal
  ;;                     :inherit 'success)


  ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; ;; === IVY FACES
  ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; (eval-after-load 'ivy  ;; seems to not work with hook
  ;;   (lambda ()
  ;;     (progn
  ;;       (set-face-attribute 'ivy-match-required-face nil
  ;;                           :weight 'normal)
  ;;       (set-face-attribute 'ivy-minibuffer-match-face-1 nil
  ;;                           :weight 'normal)
  ;;       (set-face-attribute 'ivy-minibuffer-match-face-2 nil
  ;;                           :weight 'normal)
  ;;       (set-face-attribute 'ivy-minibuffer-match-face-3 nil
  ;;                           :weight 'normal)
  ;;       (set-face-attribute 'ivy-minibuffer-match-face-4 nil
  ;;                           :weight 'normal)
  ;;       (set-face-attribute 'ivy-confirm-face nil
  ;;                           :weight 'normal)
  ;;       (set-face-attribute 'ivy-current-match nil
  ;;                           :weight 'normal)
  ;;       (set-face-attribute 'ivy-action nil
  ;;                           :weight 'normal)
  ;;       (set-face-attribute 'ivy-prompt-match nil
  ;;                           :weight 'normal)
  ;;       (set-face-attribute 'ivy-virtual nil
  ;;                           :weight 'normal))))


  ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; ;; === CUSTOM MODE FACES
  ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; (add-hook 'Custom-mode-hook
  ;;           (lambda ()
  ;;             (progn
  ;;               (set-face-attribute 'custom-face-tag nil
  ;;                                   :inherit 'default)
  ;;               (set-face-attribute 'custom-group-tag nil
  ;;                                   :inherit 'default)
  ;;               (set-face-attribute 'custom-group-tag-1 nil
  ;;                                   :inherit 'default)
  ;;               (set-face-attribute 'custom-variable-tag nil
  ;;                                   :inherit 'default)
  ;;               (set-face-attribute 'custom-button nil
  ;;                                   :box '(:line-width 2 :style released-button))
  ;;               (set-face-attribute 'custom-button-mouse nil
  ;;                                   :box '(:line-width 2 :style released-button))
  ;;               (set-face-attribute 'custom-button-pressed nil
  ;;                                   :box '(:line-width 2 :style pressed-button)))))


  ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; ;; === LATEX MODE FACES
  ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (add-hook 'LaTeX-mode-hook
            (lambda ()
              (progn
                (set-face-attribute 'font-latex-bold-face nil
                                    :foreground nil
                                    :weight 'bold
                                    :underline nil
                                    :inherit 'font-latex-string-face)
                (set-face-attribute 'font-latex-italic-face nil
                                    :foreground nil
                                    :slant 'italic
                                    :underline nil
                                    :inherit 'font-latex-string-face)
                (set-face-attribute 'font-latex-math-face nil
                                    :foreground nil   ;; MUST IF YOU WANT TO CHANGE COLOR!
                                    :weight 'normal
                                    :inherit 'tex-math)  ;; NO BOLD FOR MATH
                ;; :inherit 'font-latex-string-face)
                (set-face-attribute 'font-latex-script-char-face nil
                                    ;; === NO SEPARATE COLOR FOR _ ^
                                    :foreground nil
                                    :inherit 'font-latex-math-face)
                ;; === SECTIONS & HEADERS
                (set-face-attribute 'font-latex-sectioning-0-face nil
                                    ;; :foreground nil
                                    ;; :inherit 'font-lock-type-name-face
                                    :weight 'bold
                                    :height 1.2)
                (set-face-attribute 'font-latex-sectioning-1-face nil
                                    ;; :foreground nil
                                    ;; :inherit 'font-lock-type-name-face
                                    :weight 'bold
                                    :height 1.2)
                (set-face-attribute 'font-latex-sectioning-2-face nil
                                    :foreground nil
                                    :inherit 'font-lock-type-name-face
                                    :weight 'bold
                                    :height 1.2)
                (set-face-attribute 'font-latex-sectioning-3-face nil
                                    :foreground nil
                                    :inherit 'font-lock-type-name-face
                                    :weight 'bold
                                    :height 1.2)
                (set-face-attribute 'font-latex-sectioning-4-face nil
                                    :foreground nil
                                    :inherit 'font-lock-type-name-face
                                    :height 1.2)
                (set-face-attribute 'font-latex-sectioning-5-face nil
                                    :foreground nil
                                    :inherit 'font-lock-type-name-face
                                    :weight 'bold
                                    :height 1.0)
                (set-face-attribute 'font-latex-slide-title-face nil
                                    :foreground nil
                                    :inherit 'font-lock-type-face
                                    :weight 'bold
                                    :height 1.3)
                (set-face-attribute 'font-latex-verbatim-face nil
                                    :slant 'normal)
                ;; === SUBSCRIPTS AND SUPERSCRIPTS
                (set-face-attribute 'font-latex-subscript-face nil
                                    :foreground nil
                                    :inherit 'font-latex-math-face
                                    :height 1)
                (set-face-attribute 'font-latex-superscript-face nil
                                    :foreground nil
                                    :inherit 'font-latex-math-face
                                    :height 1)
                ;; === CUSTOM KEYWORDS = KEYWORDS
                (set-face-attribute 'font-latex-sedate-face nil
                                    :foreground nil
                                    :inherit 'font-lock-keyword-face))))

  ;; ;; ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; ;; ;; ;; === MARKDOWN MODE FACES
  ;; ;; ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (add-hook 'markdown-mode-hook
            (lambda ()
              (progn
                ;; === HEADERS, BASICALLY
                (set-face-attribute 'markdown-header-face nil
                                    :foreground nil
                                    :inherit 'font-lock-function-name-face)
                (set-face-attribute 'markdown-header-face-1 nil
                                    :foreground nil
                                    :inherit 'markdown-header-face
                                    :height 1.5)
                (set-face-attribute 'markdown-header-face-2 nil
                                    :foreground nil
                                    :inherit 'markdown-header-face
                                    :height 1.4)
                (set-face-attribute 'markdown-header-face-3 nil
                                    :foreground nil
                                    :inherit 'markdown-header-face
                                    :height 1.2)
                (set-face-attribute 'markdown-header-face-4 nil
                                    :foreground nil
                                    :inherit 'markdown-header-face
                                    :height 1.1)
                (set-face-attribute 'markdown-header-face-5 nil
                                    :foreground nil
                                    :inherit 'markdown-header-face
                                    :height 1.0))))

  ;; ;; ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; ;; ;; ;; === ORG MODE FACES
  ;; ;; ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (add-hook 'org-mode-hook
            (lambda ()
              (progn
                (set-face-attribute 'org-meta-line nil
                                    ;; === Lines starting with #+
                                    :foreground nil
                                    :inherit 'font-lock-comment-face
                                    :slant 'normal)
                ;; === SPECIFIC COLOURING FOR WOMBAT
                (set-face-attribute 'org-level-1 nil
                                    :inherit 'font-lock-function-name-face
                                    :weight 'bold
                                    :height 1.3)
                (set-face-attribute 'org-level-2 nil
                                    :inherit 'font-lock-keyword-face
                                    :weight 'bold
                                    :height 1.2)
                (set-face-attribute 'org-level-3 nil
                                    :inherit 'font-lock-preprocessor-face
                                    :weight 'bold
                                    :height 1.1)
                (set-face-attribute 'org-level-4 nil
                                    :inherit 'font-lock-preprocessor-face
                                    :weight 'bold
                                    :height 1.0)
                (set-face-attribute 'org-level-5 nil
                                    :inherit 'font-lock-preprocessor-face
                                    :weight 'bold
                                    :height 1.0)
                (set-face-attribute 'org-level-6 nil
                                    :inherit 'font-lock-preprocessor-face
                                    :weight 'bold
                                    :height 1.0)
                (set-face-attribute 'org-level-7 nil
                                    :inherit 'font-lock-preprocessor-face
                                    :weight 'bold
                                    :height 1.0)
                (set-face-attribute 'org-level-8 nil
                                    :inherit 'font-lock-preprocessor-face
                                    :weight 'bold
                                    :height 1.0)

                ;;               ;; === HEADERS AND TITLES
                (set-face-attribute 'org-document-title nil
                                    :height 1.5)
                (set-face-attribute 'org-level-1 nil
                                    :inherit 'default ;; use default face
                                    :weight 'bold
                                    :height 1.3)
                (set-face-attribute 'org-level-2 nil
                                    :inherit 'default
                                    :weight 'bold
                                    :height 1.2)
                (set-face-attribute 'org-level-3 nil
                                    :inherit 'default
                                    :weight 'bold
                                    :height 1.1)
                (set-face-attribute 'org-level-4 nil
                                    :inherit 'default
                                    :weight 'bold
                                    :height 1.0)
                (set-face-attribute 'org-level-5 nil
                                    :inherit 'default
                                    :weight 'bold)
                (set-face-attribute 'org-level-6 nil
                                    :inherit 'default
                                    :weight 'bold)
                (set-face-attribute 'org-level-7 nil
                                    :inherit 'default
                                    :weight 'bold)
                (set-face-attribute 'org-level-8 nil
                                    :inherit 'default
                                    :weight 'bold)

                ;; === BLOCKS & CODE
                (set-face-attribute 'org-block nil
                                    :inherit 'default
                                    :foreground nil)
                (set-face-attribute 'org-block-begin-line nil
                                    :box '(:line-width 1 :style released-button))
                (set-face-attribute 'org-block-end-line nil
                                    :inherit 'org-block-begin-line)
                (set-face-attribute 'org-code nil
                                    :foreground nil
                                    :inherit 'font-lock-function-name-face)
                ;; ;; === LATEX AND RELATED
                (set-face-attribute 'org-latex-and-related nil
                                    :foreground nil
                                    :inherit 'font-lock-function-name-face))))

#+END_SRC
