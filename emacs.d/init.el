(setq user-full-name "Adrian Manea")
(setq user-mail-address "adrianmanea@fastmail.fm")

;; === PACKAGE SETUP
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(eval-when-compile
  (require 'use-package))			; install manually (M-x package-install)

(setq use-package-verbose t)
(setq package-always-ensure t)
(require 'diminish)					; install separately (M-x package-install)
(require 'bind-key)					; to use :bind in use-package
(setq load-prefer-newer t)

;; === RECENT FILE LIST
(use-package recentf
  :init
  (setq recentf-auto-cleanup 'never
        recentf-max-menu-items 20
        recentf-max-saved-items 100)
  :config
  (recentf-load-list)
  :bind
  ("C-c r". recentf-open-files))

(abbrev-mode -1)
;; when you decide to use it, see here
;; http://ergoemacs.org/emacs/emacs_abbrev_mode_tutorial.html

;; backup (tilde files)
(setq backup-directory-alist '(("." . "~/.emacs.d/backups/")))
(setq delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t)
;; auto-save (# files)
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/autosaved/" t)))

;; === GLOBAL AND INTERFACE TWEAKS
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(fringe-mode 10)					; 10 pixel fringe
(setq inhibit-startup-screen t)
(global-font-lock-mode t)
(blink-cursor-mode -1)				; don't blink cursor
(set-default 'cursor-type 'box)		; box, hollow, bar, (bar . n), hbar, (hbar . n), nil
(column-number-mode)				; line number and column number in modeline
(line-number-mode)
(delete-selection-mode 1)			; write over and replace selected text
(show-paren-mode 1)					; highlight matching parens
(setq-default tab-width 4)
(setq indent-tabs-mode nil)			; convert tabs to spaces
(setq-default next-screen-context-lines 10)	; overlap for page-down
(save-place-mode)					; remember position when reopening buffer
(diminish 'eldoc-mode)				; hide ElDoc (new in v26.1)
(global-visual-line-mode)			; soft wrap text without indicators
(diminish 'visual-line-mode)	    ; hide Wrap mode

; highlight text over 80 columns
; enable whitespace-mode
(require 'whitespace)
(setq whitespace-line-column 80)
(setq whitespace-style '(face lines-tail))
(global-whitespace-mode -1)

(setq debug-on-error t)

;; bigger file name in buffer menu mode
(setq Buffer-menu-name-wdith 30)

;; === TIME AND DATE IN MODELINE
(setq display-time-format "   %a, %d %b %H:%M")
(setq display-time-default-load-average nil)		; hide CPU load
(display-time)

(use-package server
  :if window-system
  :defer 5
  :config
  (or (server-running-p) (server-mode))
  (add-hook 'server-switch-hook
            (lambda ()
              (when (current-local-map)
                (use-local-map (copy-keymap (current-local-map))))
              (local-set-key (kbd "C-x k") 'server-edit))))



;; === BUILT-IN PACKAGES

;; === DIRED STUFF
(setq directory-free-space-args "-kh" ;; human-readable free space, 1K block
      dired-listing-switches "-alhGg")
;; don't show group, human-readable space, no owner

;; === CALENDAR
(setq calendar-date-style 'european)
(setq calendar-week-start-day 1)
(setq calendar-latitude 44.439663)
(setq calendar-longitude 26.096306)
(setq calendar-location-name "Bucharest, Romania")
(setq calendar-time-zone 120) ;; UTC + 2


;; === MELPA PACKAGES

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; === THEMES AND APPEARANCE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UNDO TREE
(use-package undo-tree
  :ensure t
  :if window-system)

;; === HIDE MODE LINE (for presentations etc)
(use-package hide-mode-line
  :defer t)

;; === RAINBOW (color code highlight)
(use-package rainbow-mode
  :defer t)

;; === RAINBOW PARENS
(use-package rainbow-delimiters
  :ensure t)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
(add-hook 'LaTeX-mode-hook #'rainbow-delimiters-mode)

;; === NORD THEME
;; documentation: https://github.com/arcticicestudio/nord-emacs
(use-package nord-theme
  :if window-system
  :defer t
  :init
  (setq nord-comment-brightness 20)
  (setq nord-region-highlight 'default)) ;; 'default / "frost" / "snowstorm"

;; === SOLARIZED THEME
(use-package solarized-theme
  :if window-system
  :defer t)

(use-package all-theicons
  :ensure all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))

;; === MAGIT
(use-package magit
  :ensure t
  :bind
  ("C-x g" . nil)
  ("C-x g" . magit-list-repositories)
  :init
  (setq magit-repository-directories
		'(("~/repos/0-mine/dotfiles" . 0)
		  ("~/repos/0-mine/adrianmanea.xyz" . 0)))
  (setq magit-repolist-columns
		'(("Name"    25 magit-repolist-column-ident                  ())
          ("Version" 25 magit-repolist-column-version                ())
          ;; ("D"        1 magit-repolist-column-dirty                  ())
          ("L<U"      3 magit-repolist-column-unpulled-from-upstream ((:right-align t)))
          ("L>U"      3 magit-repolist-column-unpushed-to-upstream   ((:right-align t)))
          ("Path"    99 magit-repolist-column-path                   ()))))
;; Documentation for global list:
;; https://emacs.stackexchange.com/questions/32696/how-to-use-magit-list-repositories
;; About dirty:
;; Show N if there is at least one untracked file.
;; Show U if there is at least one unstaged file.
;; Show S if there is at least one staged file.
;; Only one letter is shown, the first that applies

(use-package markdown-mode
  :ensure t)

;; === ORG STUFF
(use-package org
  :ensure t)

;; === AVY
;; documentation: https://github.com/abo-abo/avy
(global-unset-key (kbd "M-g c"))        ;; goto char
(use-package avy
  :ensure t
  :bind
  ("M-g l" . avy-goto-line)
  ("M-g c" . avy-goto-char-2)
  ("C-c C-j" . avy-resume)) ;; previous action

;; === IVY
;; Documentation: https://oremacs.com/swiper/#key-bindings
;; more documentation: https://writequit.org/denver-emacs/presentations/2017-04-11-ivy.html
(ido-mode -1)       ; disable old ido-mode

(use-package ivy
  :ensure t
  :if window-system
  :init
  (setq ivy-count-format "%d/%d ")
  (setq ivy-use-virtual-buffers t)
  (setq enable-recursive-minibuffers t)
  (global-set-key (kbd "C-x b") 'ivy-switch-buffer)
  (global-set-key (kbd "C-c C-r") 'ivy-resume)
  (ivy-mode)
  (diminish 'ivy-mode))

;; === SPELLING
(use-package flyspell
  :ensure t
  :defer t
  :diminish flyspell-mode
  :config
  (setq ispell-program-name "aspell")
  (setq ispell-alternate-dictionary nil)
  (setq ispell-dictionary "english")
  (setq ispell-local-dictionary nil)
  (defun fd-switch-dictionary()
    "Switch between English and Romanian dicts"
    (interactive)
    (let* ((dic ispell-current-dictionary)
           (change (if (string= dic "english") "ro-classic" "english")))
      (ispell-change-dictionary change)
      (message "Dictionary switched from %s to %s" dic change)
      ))
  (defun my-save-word ()
    "Add word to dictionary (Learn)"
    (interactive)
    (let ((current-location (point))
          (word (flyspell-get-word)))
      (when (consp word)
        (flyspell-do-correct 'save nil
                             (car word) current-location (cadr word)
                             (caddr word) current-location))))
  (global-set-key (kbd "C-c fly") 'flyspell-mode)
  (define-key flyspell-mode-map (kbd "C-c fs") 'my-save-word)
  (define-key flyspell-mode-map (kbd "C-c er") 'fd-switch-dictionary)
  (define-key flyspell-mode-map (kbd "C-M-i") nil)
  (define-key flyspell-mode-map (kbd "M-TAB") nil)
  (define-key flyspell-mode-map (kbd "C-;") 'flyspell-auto-correct-previous-word)
  (define-key flyspell-mode-map (kbd "C-,") nil))

;; === PDF TOOLS
(use-package pdf-tools  ;; install separately (via package manager)
  :pin manual           ;; manually update
  :defer t
  :config
  ;; initialise
  (pdf-loader-install)
  (setq-default pdf-view-display-size 'fit-width)
  ;; turn off cua so copy works
  (add-hook 'pdf-view-mode-hook
            (lambda () (cua-mode 0)))
  ;; more fine-grained zoom
  (setq pdf-view-resize-factor 1.1)
  ;; continuous scroll in pdf-view
  (setq pdf-view-continuous t)
  (add-hook 'pdf-view-mode-hook (lambda ()
                                  (pdf-view-midnight-minor-mode)))
  ;; === PDF VIEW COLORS (TEXT . BG)
  (setq pdf-view-midnight-colors (quote ("#D8DEE9" . "#2E3440")))
		;; simple-2 (quote ("#fffbe0" . "#222222")))
  ;; monokai        (quote ("#fcfcfa" . "#2d2a2e")))
                                        ; automatically turns on midnight-mode for pdfs
  (add-to-list 'auto-mode-alist '("\\.pdf$\\'" . pdf-view-mode)))


;; === IMAGES & PDFs
;; uses imagemagick
(eval-after-load 'image '(use-package image+ :defer t))
(eval-after-load 'image+ '(imagex-global-sticky-mode 1))
(add-hook 'image-mode-hook
          (lambda ()
            (imagex-auto-adjust-mode t)
            (imagex-global-sticky-mode)))

(use-package auctex
  :defer t      ;; MUST!!
  :ensure t)

;; use pdfview with auctex
(setq TeX-view-program-selection '((output-pdf "pdf-tools")))
(setq TeX-view-program-list '(("pdf-tools" "TeX-pdf-tools-sync-view")))
(setq TeX-source-correlate-mode t)
(setq TeX-source-correlate-start-server t)
(setq TeX-source-correlate-method (quote synctex))
(setq TeX-PDF-mode t)
(setq TeX-fold-mode 1)
;; automatically reload pdf after compilation
(add-hook 'TeX-after-compilation-finished-functions
          #'TeX-revert-document-buffer)

(add-hook 'doc-view-mode-hook
          (lambda ()
            (pdf-tools-install)))

(use-package reftex
  :ensure t
  :defer t
  :commands turn-on-reftex
  :diminish reftex-mode
  :init
  (progn
    (setq reftex-plug-into-AUCTeX t)
    (setq reftex-plug-into-auctex t)
    (setq reftex-extra-bindings t)))
;; C-c t    reftex-toc
;; C-c l    reftex-label
;; C-c r    reftex-reference
;; C-c c    reftex-citation
;; C-c v    reftex-view-crossref
;; C-c s    reftex-search-document
;; C-c g    reftex-grep-document

;; Turn on RefTeX for AUCTeX, http://www.gnu.org/s/auctex/manual/reftex/reftex_5.html
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
;; Make RefTeX interact with AUCTeX, http://www.gnu.org/s/auctex/manual/reftex/AUCTeX_002dRefTeX-Interface.html
(setq reftex-plug-into-AUCTeX t)

(setq font-latex-fontify-script nil)
;; super-/sub-script on baseline
(setq font-latex-script-display (quote (nil)))

(defun get-bibtex-keys (file)
  (with-current-buffer (find-file-noselect file)
    (mapcar 'car (bibtex-parse-keys))))

(defun LaTeX-add-all-bibitems-from-bibtex ()
  (interactive)
  (mapc 'LaTeX-add-bibitems
        (apply 'append
               (mapcar 'get-bibtex-keys (reftex-get-bibfile-list)))))

;; === YASNIPPET
(use-package yasnippet
  :ensure t
  :defer t)
(add-hook 'LaTeX-mode-hook
          (lambda ()
            (yas-minor-mode)
            (diminish 'yas-minor-mode)
            (add-to-list 'yas-snippet-dirs "~/.emacs.d/mine/snips/")
            (setq yas-indent-line 'auto)
            (yas-reload-all)))

;; === COMPANY COMPLETE
;; http://company-mode.github.io/
(global-unset-key (kbd "C-3")) ;; numeric prefix, will be used for Company
(use-package company
  :ensure t
  :diminish company-mode
  :init
  (setq company-selection-wrap-arround t
        company-show-numbers t
        company-tooltip-flip-when-above t
        company-auto-complete nil
        company-idle-delay nil)
  :hook ((merlin-mode . company-mode)
         (coq-mode . company-coq-mode))
  :bind (("C-c c o" . company-mode)   ;; global keybindings
         ("C-3" . company-complete)
         (:map company-active-map
               ("C-n" . company-select-next)
               ("C-p" . company-select-previous))))
;; Use numbers 0-9 (in addition to M-<num>) to select company completion candidates
;; (mapc (lambda (x)
;;         (define-key company-active-map (format "%d" x) 'company-complete-number))
;;       (number-sequence 0 9)))

;; === LISP (SBCL w/ SLIME)
;; (require 'slime-autoloads)
(use-package slime
  :defer t
  :config
  (setq inferior-lisp-program "/usr/bin/sbcl" ; Steel Bank Common Lisp
        slime-contribs '(slime-fancy)))

(use-package slime-company
  :ensure slime
  :ensure company
  :defer t)

;; === ELFEED
;; more tips and tricks: https://nullprogram.com/blog/2013/11/26/
;; extra-features: https://nullprogram.com/blog/2015/12/03/
;; "s" for search filter
(use-package elfeed
  :ensure t
  :defer t
  :config
  (setq elfeed-db-directory "~/.emacs.d/elfeed-db")
  ;; put feeds separately
  (load "~/.emacs.d/feeds.el")
  ;; play yt videos with mpv
  ;; taken from here https://www.reddit.com/r/emacs/comments/7usz5q/youtube_subscriptions_using_elfeed_mpv_no_browser/
  (defun elfeed-play-with-mpv ()
    "Play entry link with mpv."
    (interactive)
    (let ((entry (if (eq major-mode 'elfeed-show-mode) elfeed-show-entry (elfeed-search-selected :single)))
          (quality-arg "")
          (quality-val (completing-read "Max height resolution (0 for unlimited): " '("0" "480" "720") nil nil)))
      (setq quality-val (string-to-number quality-val))
      (message "Opening %s with height≤%s with mpv..." (elfeed-entry-link entry) quality-val)
      (when (< 0 quality-val)
        (setq quality-arg (format "--ytdl-format=[height<=?%s]" quality-val)))
      (start-process "elfeed-mpv" nil "mpv" quality-arg (elfeed-entry-link entry))))
  (defvar elfeed-mpv-patterns
    '("youtu\\.?be")
    "List of regexp to match against elfeed entry link to know
    whether to use mpv to visit the link.")
  (defun elfeed-visit-or-play-with-mpv ()
    "Play in mpv if entry link matches `elfeed-mpv-patterns', visit otherwise.
    See `elfeed-play-with-mpv'."
    (interactive)
    (let ((entry (if (eq major-mode 'elfeed-show-mode) elfeed-show-entry (elfeed-search-selected :single)))
          (patterns elfeed-mpv-patterns))
      (while (and patterns (not (string-match (car elfeed-mpv-patterns) (elfeed-entry-link entry))))
        (setq patterns (cdr patterns)))
      (if patterns
          (elfeed-play-with-mpv)
        (if (eq major-mode 'elfeed-search-mode)
            (elfeed-search-browse-url)
          (elfeed-show-visit)))))
  (define-key elfeed-search-mode-map (kbd "v")
    (lambda ()
      (interactive)
      (elfeed-play-with-mpv)))
  (define-key elfeed-show-mode-map (kbd "v")
    (lambda ()
      (interactive)
      (elfeed-play-with-mpv)))
  (defun elfeed-mark-all-as-read ()
    "Mark currently shown articles read"
    (interactive)
    (mark-whole-buffer)
    (elfeed-search-untag-all-unread))
  (define-key elfeed-search-mode-map (kbd "A") 'elfeed-mark-all-as-read)
  (define-key elfeed-search-mode-map (kbd "R") 'elfeed-search-fetch) ;; refresh
  (setq elfeed-search-title-max-width '100)
  (setq elfeed-search-title-trailing-width '30)       ;; space left for tags & titles
  (setq elfeed-search-title-min-width '10)
  (setq elfeed-search-date-format '("%d %b" 6 :left)) ;; 25 Mar
  (setq elfeed-search-filter "@3-days-ago +unread")   ;; default filter at startup
  (defun elfeed-open-in-ff ()
    "Open link in Firefox rather than eww"
    (interactive)
    (let ((browse-url-generic-program "firefox"))
      (elfeed-show-visit t)))
  (define-key elfeed-show-mode-map (kbd "f") 'elfeed-open-in-ff)
  (defun elfeed-open-in-next ()
    "Open link in Next browser"
    (interactive)
    (let ((browse-url-generic-program "next"))
      (elfeed-show-visit t)))
  (define-key elfeed-show-mode-map (kbd "x") 'elfeed-open-in-next)
  (define-key elfeed-search-mode-map (kbd "t") 'elfeed-search-set-feed-title))
;; DEFAULT SEARCH-MODE KEYBINDINGS
;; (define-key map "q" 'elfeed-search-quit-window)
;; (define-key map "g" 'elfeed-search-update--force)
;; (define-key map "G" 'elfeed-search-fetch)
;; (define-key map (kbd "RET") 'elfeed-search-show-entry)
;; (define-key map "s" 'elfeed-search-live-filter)
;; (define-key map "S" 'elfeed-search-set-filter)
;; (define-key map "b" 'elfeed-search-browse-url)
;; (define-key map "y" 'elfeed-search-yank)
;; (define-key map "u" 'elfeed-search-tag-all-unread)
;; (define-key map "r" 'elfeed-search-untag-all-unread)
;; (define-key map "n" 'next-line)
;; (define-key map "p" 'previous-line)
;; (define-key map "+" 'elfeed-search-tag-all)
;; (define-key map "-" 'elfeed-search-untag-all)

(use-package eww
  :ensure t
  :if window-system
  :init
  (setq browse-url-browser-function 'eww-browse-url))
;; eww as default browser -- "&" to open in $BROWSER
;; === EWW KEYBINDINGS ===
;; g = eww-reload
;; w = eww-copy-page-url
;; M-RET = eww-open-in-new-buffer
;; R = eww-readable = extract text
;; F = eww-toggle-fonts (whether to use variable pitch fonts or not)
;; M-C = eww-toggle-colors (whether to use HTML-specified colors or not)
;; d = eww-download = download the URL under the point
;; l = eww-back-url
;; r = eww-forward-url
;; H = eww-list-histories
;; b = eww-add-bookmark
;; B = eww-list-bookmarks
;; S = eww-list-buffers (view all eww buffers open)
;; s = eww-switch-to-buffer (via a minibuffer prompt)

;; === EMAIL (MU4E)
(use-package mu4e
  :defer t
  :if window-system
  :load-path "/usr/share/emacs/site-lisp/mu4e"
  :ensure org-mime
  :ensure htmlize
  :ensure mu4e-alert
  :init
  ;; (require 'mu4e)
  (require 'org-mu4e)
  ;; convert org messages to html
  (require 'smtpmail)
  ;; source: http://www.macs.hw.ac.uk/~rs46/posts/2014-01-13-mu4e-email-client.html
  (setq message-send-mail-function 'smtpmail-send-it
        smtpmail-starttls-credentials
        '(("smtp.fastmail.com" 587 nil nil))
        smtpmail-default-smtp-server "smtp.fastmail.com"
        smtpmail-smtp-server "smtp.fastmail.com"
        smtpmail-smtp-service 587
        smtpmail-smtp-user "adrianmanea@fastmail.fm"
        smtpmail-debug-info t
        mu4e-sent-messages-behavior 'sent
        mu4e-hide-index-messages t        ;; don't show "Indexing..."
        mu4e-index-update-in-background t
        mu4e-index-update-error-warning nil ;; don't show nonzero exit code
        mu4e-use-fancy-chars nil)           ;; show fancy (Unicode) chars for marks
  ;; lazy indexing => speedup
  (setq
   mu4e-index-cleanup nil
   mu4e-index-lazy-check t)
  (setq org-mu4e-convert-to-html t)
  (setq mu4e-maildir (expand-file-name "~/email/mbsyncmail"))
  (setq mu4e-drafts-folder "/Drafts")
  (setq mu4e-sent-folder "/Sent")
  (setq mu4e-trash-folder "/Trash")
  ;; get mail
  (setq mu4e-get-mail-command "mbsync -c ~/.emacs.d/.mbsyncrc -a"
        mu4e-html2text-command "w3m -T text/html"
        ;; comment /usr/share/emacs/site-lisp/mu4e/mu4e-message.el (require 'html2text)
        mu4e-update-interval 300 ;; in seconds
        mu4e-headers-auto-update t
        mu4e-compose-signature-auto-include nil)
  (setq mu4e-maildir-shortcuts
        '(("/INBOX" . ?i)
          ("/Sent" . ?s)
          ("/Trash" . ?t)
          ("/Drafts" . ?d)))
  ;; headers to show in mu4e
  ;; header . characters
  (setq mu4e-headers-date-format "%d %b %Y"
        mu4e-headers-time-format "%H:%M")
  (setq mu4e-headers-fields
        '((:date . 15)
          (:flags . 5)
          (:from-or-to . 30)
          (:thread-subject . nil))) ;; unlimited
  ;; don't keep message buffers around
  (setq message-kill-buffer-on-exit t)
  ;; show-images
  (setq mu4e-show-images t)
  ;; use imagemagick, if available
  (when (fboundp 'imagemagick-register-types)
    (imagemagick-register-types))
  ;; don't inherit content from emacs-mail
  (setq mu4e-reply-to-address "adrianmanea@fastmail.fm"
        user-mail-address "adrianmanea@fastmail.fm"
        user-full-name "Adrian Manea")
  (add-hook 'mu4e-compose-mode-hook
            '(lambda()
               (turn-on-auto-fill)
               (set-fill-column 80)))
  ;; Open message in browser
  (defun mu4e-msgv-action-view-in-browser (msg)
    "View the body of the message in a web browser."
    (interactive)
    (let ((html (mu4e-msg-field (mu4e-message-at-point t) :body-html))
          (tmpfile (format "%s/%d.html" temporary-file-directory (random))))
      (unless html (error "No html part for this message"))
      (with-temp-file tmpfile
        (insert
         "<html>"
         "<head><meta http-equiv=\"content-type\""
         "content=\"text/html;charset=UTF-8\">"
         html))
      (browse-url (concat "file://" tmpfile))))
  (add-hook 'mu4e-view-mode-hook
            (lambda()
              (add-to-list 'mu4e-view-actions
                           '("View in browser" . mu4e-msgv-action-view-in-browser) t)))
  ;; press "a" to open available actions
  (require 'gnus-dired)
  ;; make the `gnus-dired-mail-buffers' function also work on
  ;; message-mode derived modes, such as mu4e-compose-mode
  ;; from here https://zmalltalker.com/linux/mu.html
  (defun gnus-dired-mail-buffers ()
    "Return a list of active message buffers."
    (let (buffers)
      (save-current-buffer
        (dolist (buffer (buffer-list t))
          (set-buffer buffer)
          (when (and (derived-mode-p 'message-mode)
                     (null message-sent-message-via))
            (push (buffer-name buffer) buffers))))
      (nreverse buffers)))
  (setq gnus-dired-mail-mode 'mu4e-user-agent)
  (add-hook 'dired-mode-hook 'turn-on-gnus-dired-mode)
  ;; dired C-c RET C-a = attach file
  (add-hook 'after-init-hook #'mu4e-alert-enable-mode-line-display))

;; (use-package org-mime)
;; ;; documentation: https://github.com/hniksic/emacs-htmlize
;; (use-package htmlize)

;; === MU4E-ALERT: New message notification
;; details: https://github.com/iqbalansari/mu4e-alert
(use-package mu4e-alert
  :ensure t
  :defer t)

;; === MU4E Thread Folding
;; Fold/Unfold all threads with "P"
;; source: https://gist.github.com/felipeochoa/614308ac9d2c671a5830eb7847985202
;; Refactored from https://github.com/djcb/mu/pull/783

(defun mu4e~headers-msg-unread-p (msg)
  "Check if MSG is unread."
  (let ((flags (mu4e-message-field msg :flags)))
    (and (member 'unread flags) (not (member 'trashed flags)))))

(defvar mu4e-headers-folding-slug-function
  (lambda (headers) (format " (%d)" (length headers)))
  "Function to call to generate the slug that will be appended to folded threads.
This function receives a single argument HEADERS, which is a list
of headers about to be folded.")

(defun mu4e~headers-folded-slug (headers)
  "Generate a string to append to the message line indicating the fold status.
HEADERS is a list with the messages being folded (including the root header)."
  (funcall mu4e-headers-folding-slug-function headers))

(defun mu4e~headers-fold-make-overlay (beg end headers)
  "Hides text between BEG and END using an overlay.
HEADERS is a list with the messages being folded (including the root header)."
  (let ((o (make-overlay beg end)))
    (overlay-put o 'mu4e-folded-thread t)
    (overlay-put o 'display (mu4e~headers-folded-slug headers))
    (overlay-put o 'evaporate t)
    (overlay-put o 'invisible t)))

(defun mu4e~headers-fold-find-overlay (loc)
  "Find and return the 'mu4e-folded-thread overlay at LOC, or return nil."
  (cl-dolist (o (overlays-in (1- loc) (1+ loc)))
    (when (overlay-get o 'mu4e-folded-thread)
      (cl-return o))))

(defun mu4e-headers-fold-all ()
  "Fold all the threads in the current view."
  (interactive)
  (let ((thread-id "") msgs fold-start fold-end)
    (mu4e-headers-for-each
     (lambda (msg)
       (end-of-line)
       (push msg msgs)
       (let ((this-thread-id (mu4e~headers-get-thread-info msg 'thread-id)))
         (if (string= thread-id this-thread-id)
             (setq fold-end (point))
           (when (< 1 (length msgs))
             (mu4e~headers-fold-make-overlay fold-start fold-end (nreverse msgs)))
           (setq fold-start (point)
                 fold-end (point)
                 msgs nil
                 thread-id this-thread-id)))))
    (when (< 1 (length msgs))
      (mu4e~headers-fold-make-overlay fold-start fold-end (nreverse msgs)))))

(defun mu4e-headers-toggle-thread-folding (&optional subthread)
  "Toggle the folding state for the thread at point.
If SUBTHREAD is non-nil, only fold the current subthread."
  ;; Folding is accomplished using an overlay that starts at the end
  ;; of the parent line and ends at the end of the last descendant
  ;; line. If there's no overlay, it means it isn't folded
  (interactive "P")
  (if-let ((o (mu4e~headers-fold-find-overlay (point-at-eol))))
		  (delete-overlay o)
		  (let* ((msg (mu4e-message-at-point))
				 (thread-id (mu4e~headers-get-thread-info msg 'thread-id))
				 (path-re (concat "^" (mu4e~headers-get-thread-info msg 'path)))
				 msgs first-marked-point last-marked-point)
			(mu4e-headers-for-each
			 (lambda (submsg)
			   (when (and (string= thread-id (mu4e~headers-get-thread-info submsg 'thread-id))
						  (or (not subthread)
							  (string-match-p path-re (mu4e~headers-get-thread-info submsg 'path))))
				 (push msg msgs)
				 (setq last-marked-point (point-at-eol))
				 (unless first-marked-point
				   (setq first-marked-point last-marked-point)))))
			(when (< 1 (length msgs))
			  (mu4e~headers-fold-make-overlay first-marked-point last-marked-point (nreverse msgs))))))





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; === GLOBAL KEYBINDINGS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define-key global-map (kbd "C-c f f") 'toggle-frame-fullscreen)
(global-unset-key (kbd "C-z")) ;; minimize (CAUSES HANG depending on WM!)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; === CUSTOM FUNCTIONS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; === COPY BUFFER NAME
(defun my-put-file-name-on-clipboard ()
  "Put the current file name on the clipboard"
  (interactive)
  (let ((filename (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name))))
    (when filename
      (with-temp-buffer
        (insert filename)
        (clipboard-kill-region (point-min) (point-max)))
      (message filename))))

;; ===RENAME FILE AND BUFFER
(defun rename-file-and-buffer (new-name)
  "Renames both current buffer and file it's visiting to NEW-NAME."
  (interactive "sNew name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer new-name)
          (message "A buffer named '%s' already exists!" new-name)
        (progn
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil))))))

;; === SWAP WINDOWS IN SPLIT
(defun win-swap ()
  "Swap windows using buffer-move.el" ;; it seems it's built-in
  (interactive)
  (if
      (null (windmove-find-other-window 'right))
      (buf-move-left)
    (buf-move-right)))

;; === PASTE IN TERMINAL
;; S-Insert by default
(add-hook 'term-mode-hook
          (lambda ()
            (define-key term-raw-map (kbd "C-y") 'term-paste)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; === FIX FOR CONTINUOUS PDF-VIEW SCROLL-OTHER-WINDOW
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar-local sow-scroll-up-command nil)

(defvar-local sow-scroll-down-command nil)

(defvar sow-mode-map
  (let ((km (make-sparse-keymap)))
    (define-key km [remap scroll-other-window] 'sow-scroll-other-window)
    (define-key km [remap scroll-other-window-down] 'sow-scroll-other-window-down)
    km)
  "Keymap used for `sow-mode'")

(define-minor-mode sow-mode
  "FIXME: Not documented."
  nil nil nil
  :global t)

(defun sow-scroll-other-window (&optional arg)
  (interactive "P")
  (sow--scroll-other-window-1 arg))

(defun sow-scroll-other-window-down (&optional arg)
  (interactive "P")
  (sow--scroll-other-window-1 arg t))

(defun sow--scroll-other-window-1 (n &optional down-p)
  (let* ((win (other-window-for-scrolling))
         (cmd (with-current-buffer (window-buffer win)
				(if down-p
					(or sow-scroll-down-command #'scroll-up-command)
				  (or sow-scroll-up-command #'scroll-down-command)))))
    (with-current-buffer (window-buffer win)
      (save-excursion
        (goto-char (window-point win))
        (with-selected-window win
          (funcall cmd n))
        (set-window-point win (point))))))

(add-hook 'Info-mode-hook
		  (lambda nil
			(setq sow-scroll-up-command
				  (lambda (_) (Info-scroll-up))
				  sow-scroll-down-command
				  (lambda (_) (Info-scroll-down)))))

(add-hook 'doc-view-mode-hook
          (lambda nil
            (setq sow-scroll-up-command
                  'doc-view-scroll-up-or-next-page
                  sow-scroll-down-command
                  'doc-view-scroll-down-or-previous-page)))

(add-hook 'pdf-view-mode-hook
          (lambda nil
            (setq sow-scroll-up-command
                  'pdf-view-scroll-up-or-next-page
                  sow-scroll-down-command
                  'pdf-view-scroll-down-or-previous-page)))

(provide 'scroll-other-window)
;;; scroll-other-window.el ends here
(global-unset-key (kbd "C-M-v")) ;; original scroll other window DOWN
(global-unset-key (kbd "M-<prior>"))
(global-unset-key (kbd "C-M-S-v")) ;; original scroll other window UP
(global-unset-key (kbd "M-<next>"))
(define-key global-map (kbd "M-<prior>") 'sow-scroll-other-window-down)
(define-key global-map (kbd "M-<next>") 'sow-scroll-other-window)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; === MY PACKAGES
(add-to-list 'custom-theme-load-path "~/.emacs.d/mine/thm/monok")
(add-to-list 'custom-theme-load-path "~/.emacs.d/mine/thm/old")
(add-to-list 'custom-theme-load-path "~/.emacs.d/mine/thm/light")
(add-to-list 'custom-theme-load-path "~/.emacs.d/mine/thm/sol")
(add-to-list 'custom-theme-load-path "~/.emacs.d/mine/thm/simple")

(when window-system
  ;; (load-theme 'solarized-dark t))
  (load-theme 'simple-nord t))


;; === FONT (FACES) SETTINGS
;; === BITMAP ===
;; (set-frame-font "Misc Fixed 12")
;; (set-frame-font "Dina")
;; (set-frame-font "UW Ttyp0 13")
;; (set-frame-font "xos4 Terminus 12")
;; (set-frame-font "Fixedsys Excelsior 12")
;; (set-frame-font "DOSEGA 13")

;; === TRUETYPE ===
;; (set-frame-font "Liberation Mono 11")
;; (set-frame-font "Consolas 12")
;; (set-frame-font "Input Mono Narrow 11")
;; (set-frame-font "DejaVu Sans Mono 11")
;; (set-frame-font "Droid Sans Mono 11")
;; (set-frame-font "Anonymous Pro 12")
;; (set-frame-font "Cousine 11")
;; (set-frame-font "IBM Plex Mono 11")

;; === NARROW ===
(set-frame-font "PragmataPro Mono 12")
;; (set-frame-font "M+ 1m 12")
;; (set-frame-font "Iosevka 12")

(setq-default line-spacing 0)





(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
	(yasnippet solarized-theme markdown-mode mu4e-alert htmlize org-mime mu4e elfeed slime company auctex pdf-tools ivy avy magit undo-tree all-the-icons-dired nord-theme rainbow-delimiters rainbow-mode hide-mode-line diminish use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
