(setq user-full-name "Adrian Manea")
(setq user-mail-address "adrianmanea@fastmail.fm")

;; === PACKAGE SETUP
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))

;; (add-to-list 'package-archives
;;              '("org" . "https://orgmode.org/elpa/") t)
(package-initialize)


(eval-when-compile
  (require 'use-package))
(setq use-package-verbose t)
(setq use-package-always-ensure t)
(require 'diminish)
(require 'bind-key)				;; to use :bind in use-package
(setq load-prefer-newer t)      ;; Avoid out-dated byte-compiled Elisp files.

;; === RECENT FILE LIST
(use-package recentf
  :init
  (setq recentf-auto-cleanup 'never
        recentf-max-menu-items 20
        recentf-max-saved-items 100)
  :config
  (recentf-load-list)
  :bind
  ("C-c r" . recentf-open-files))
;; unbind C-x r r to make it an option

;; flash the frame when an error occurs
(setq visible-bell nil)

(abbrev-mode -1)
;; when you decide to use it, see here
;; http://ergoemacs.org/emacs/emacs_abbrev_mode_tutorial.html

;; backup (tilde files)
(setq backup-directory-alist '(("." . "~/.emacs.d/backups/")))
(setq delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t)
;; auto-save (# files)
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/autosaved/" t)))

;; reload file after changes with F5
;; g is used in other modes (e.g. dired)
(global-set-key (kbd "<f5>") 'revert-buffer)
(global-auto-revert-mode nil)


;; custom info files
(add-to-list 'Info-default-directory-list "~/.emacs.d/mine/info")

;; === GLOBAL AND INTERFACE TWEAKS
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(global-display-line-numbers-mode -1) ;; don't show line numbers
(fringe-mode 10)				;; 10 pixel fringe
(setq inhibit-startup-screen t)
(global-font-lock-mode t)
(blink-cursor-mode -1)			;; (don't) blink cursor
(set-default 'cursor-type 'box) ;; box, hollow, bar, (bar . n), hbar, (hbar . n), nil
(column-number-mode)			;; show column number in modeline
(line-number-mode)				;; show line number in modeline
(delete-selection-mode 1)		;; write over and replace selected text
(setq-default indicate-buffer-boundaries 'left)	;; show buffer boundaries in left fringe
(show-paren-mode 1)				;; highlight matching parens
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil) ;; convert tabs to spaces
(setq-default next-screen-context-lines 20)     ;; overlap for page-down
(save-place-mode)       ;; remember position when reopening buffer
(diminish 'eldoc-mode)  ;; hide ElDoc (new in v26.1)
(global-visual-line-mode) ;; soft wrap text without indicators
(diminish 'visual-line-mode)
(setq debug-on-error t)

;; bigger file name in buffer menu mode
(setq Buffer-menu-name-width 30)


;; === TIME AND DATE IN MODELINE
(setq display-time-format "%a, %d %b %H:%M")
(setq display-time-default-load-average nil)    ;; hide CPU load
(display-time)

;; === MELPA PACKAGES

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; === THEMES & APPEARANCE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; hide mode-line (for presentations, e.g.)
(use-package hide-mode-line
  :defer t)

;; === RAINBOW (color code highlight)
(use-package rainbow-mode
  :defer t)

;; === RAINBOW PARENS
(use-package rainbow-delimiters
  :if window-system
  :ensure t)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
(add-hook 'LaTeX-mode-hook #'rainbow-delimiters-mode)

(use-package nyan-mode
  :if window-system  ;; only load in GUI Emacs
  :defer t
  :init
  (setq nyan-bar-length 12
        nyan-cat-face-number 2
        nyan-wavy-trail t))

;; documentation: http://joostkremers.github.io/pandoc-mode/
(use-package pandoc
  :defer t)

;; documentation: https://github.com/arcticicestudio/nord-emacs
(use-package nord-theme
  :if window-system
  :defer t
  :init
  (setq nord-comment-brightness 20)
  (setq nord-region-highlight 'default)) ;; 'default / "frost" / "snowstorm"

;; documentation https://github.com/hlissner/emacs-doom-themes
(use-package doom-themes
  :if window-system
  :defer t)

(use-package solarized-theme
  :if window-system
  :defer t
  :init
  (setq solarized-use-variable-pitch nil))

(use-package all-the-icons
  :ensure all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))
  

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-to-list 'custom-theme-load-path "~/.emacs.d/mine/thm/monok")
(add-to-list 'custom-theme-load-path "~/.emacs.d/mine/thm/old")
(add-to-list 'custom-theme-load-path "~/.emacs.d/mine/thm/light")
(add-to-list 'custom-theme-load-path "~/.emacs.d/mine/thm/sol")
(add-to-list 'custom-theme-load-path "~/.emacs.d/mine/thm/simple")

(when window-system
  (progn
    (nyan-mode)
    (load-theme 'simple-nord t)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package server
  :if window-system
  :defer 5
  :config
  (or (server-running-p) (server-mode))
  (add-hook 'server-switch-hook
            (lambda ()
              (when (current-local-map)
                (use-local-map (copy-keymap (current-local-map))))
              (local-set-key (kbd "C-x k") 'server-edit))))


(use-package json-mode
  :defer t)

(use-package undo-tree
  :if window-system)

;; === NOV.el --> reading epubs
;; source: https://github.com/wasamasa/nov.el
(use-package nov
  :defer t)
(eval-after-load 'nov
  (add-to-list 'auto-mode-alist '("\\.epub$\\'" . nov-mode)))



;; === DJVU.el
;; documentation: https://elpa.gnu.org/packages/djvu.html
(use-package djvu
  :defer t
  :init
  (defun djvu-find-file-noselect (f-f-n filename &rest args)
    "If FILENAME is a Djvu file call `djvu-find-file'."
    (if (string-match "\\.djvu\\'" (file-name-sans-versions filename))
        (djvu-find-file filename nil nil t)
      (apply f-f-n filename args)))
  (advice-add 'find-file-noselect :around #'djvu-find-file-noselect))
;; keybindings:
;; g = goto page
;; v = reopen
;; i = view page as image
;; o = outline mode

(global-set-key (kbd "C-\\") nil)

(use-package multiple-cursors
  :bind
  ("C-\\" . mc/mark-next-like-this))

;; === DIRED STUFF
(setq directory-free-space-args "-kh" ;; human-readable free space, 1K block
      dired-listing-switches "-alhGg")
;; don't show group, human-readable space, no owner

;; === MAGIT
;; manual (html): https://magit.vc/manual/magit/
;; manual (pdf) : https://magit.vc/manual/magit.pdf
;; short how-to : https://magit.vc/screenshots/
(use-package magit
  :ensure t
  :bind
  ("C-x g" . nil)
  ("C-x g" . magit-list-repositories)
  :init
  (setq magit-repository-directories
        '(("~/dotfiles" . 0)
          ("~/.emacs.d" . 0)
          ("~/repos/0-mine/adrianmanea.xyz" . 0)
          ("~/repos/0-mine/cript-av" . 0)
          ("~/repos/0-mine/homalg" . 0)
          ("~/repos/0-mine/my-dotfiles" . 0)
          ("~/repos/0-mine/sla" . 0)
          ("~/teach" . 0)
          ("~/learn" . 0)
          ("~/notes" . 0)
          ("~/scripts" . 0)
          ("~/blog" . 0)
          ("~/write" . 0)))
  (setq magit-repolist-columns
        '(("Name"    25 magit-repolist-column-ident                  ())
          ("Version" 25 magit-repolist-column-version                ())
          ("D"        1 magit-repolist-column-dirty                  ())
          ("L<U"      3 magit-repolist-column-unpulled-from-upstream ((:right-align t)))
          ("L>U"      3 magit-repolist-column-unpushed-to-upstream   ((:right-align t)))
          ("Path"    99 magit-repolist-column-path                   ()))))
;; Documentation for global list:
;; https://emacs.stackexchange.com/questions/32696/how-to-use-magit-list-repositories
;; About dirty:
;; Show N if there is at least one untracked file.
;; Show U if there is at least one unstaged file.
;; Show S if there is at least one staged file.
;; Only one letter is shown, the first that applies

;; === WEATHER
;; (use-package wttrin
;;   :defer t
;;   :init
;;   (setq wttrin-default-cities '("Bucharest" "Buzau"))
;;   (setq wttrin-default-accept-language '("Accept-Language" . "en-US")))


;; === TWITTER
;; details: https://github.com/hayamiz/twittering-mode
;; (use-package twittering-mode
;;   :defer t
;;   :config
;;   (setq twittering-icon-mode t)
;;   (setq twittering-number-of-tweets-on-retrieval 50)
;;   (twittering-enable-unread-status-notifier)
;;   (setq twittering-display-remaining t))
  

;; === ORG STUFF
(use-package org
  :if window-system)


;; === CALENDAR
(setq calendar-date-style 'european)
(setq calendar-week-start-day 1)
(setq calendar-latitude 44.439663)
(setq calendar-longitude 26.096306)
(setq calendar-location-name "Bucharest, Romania")
(setq calendar-time-zone 120) ;; UTC + 2



;; === ORG-REF
;; documentation: https://github.com/jkitchin/org-ref
(use-package org-ref
  :ensure helm
  :defer t)

;; === AVY
;; documentation: https://github.com/abo-abo/avy
(global-unset-key (kbd "M-g c"))        ;; goto char
(use-package avy
  :ensure t
  :bind
  ("M-g l" . avy-goto-line)
  ("M-g c" . avy-goto-char-2)
  ("C-c C-j" . avy-resume)) ;; previous action

;; === IVY
;; Documentation: https://oremacs.com/swiper/#key-bindings
;; more documentation: https://writequit.org/denver-emacs/presentations/2017-04-11-ivy.html
(ido-mode -1)       ;; disable old ido-mode

(use-package ivy
  :if window-system
  :init
  (setq ivy-count-format "%d/%d ")
  (setq ivy-use-virtual-buffers t)
  (setq enable-recursive-minibuffers t)
  (global-set-key (kbd "C-x b") 'ivy-switch-buffer)
  (global-set-key (kbd "C-c C-r") 'ivy-resume)
  (ivy-mode)
  (diminish 'ivy-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; === SPELLING (FLYSPELL)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package flyspell
  :defer t
  :diminish flyspell-mode
  :config
  (setq ispell-program-name "aspell")
  (setq ispell-alternate-dictionary nil)
  (setq ispell-dictionary "english")
  (setq ispell-local-dictionary nil)
  (defun fd-switch-dictionary()
    "Switch between English and Romanian dicts"
    (interactive)
    (let* ((dic ispell-current-dictionary)
           (change (if (string= dic "english") "ro-classic" "english")))
      (ispell-change-dictionary change)
      (message "Dictionary switched from %s to %s" dic change)
      ))
  (defun my-save-word ()
    "Add word to dictionary (Learn)"
  (interactive)
  (let ((current-location (point))
         (word (flyspell-get-word)))
    (when (consp word)
      (flyspell-do-correct 'save nil
                           (car word) current-location (cadr word)
                           (caddr word) current-location))))
  (global-set-key (kbd "C-c fly") 'flyspell-mode)
  (define-key flyspell-mode-map (kbd "C-c fs") 'my-save-word)
  (define-key flyspell-mode-map (kbd "C-c er") 'fd-switch-dictionary)
  (define-key flyspell-mode-map (kbd "C-M-i") nil)
  (define-key flyspell-mode-map (kbd "M-TAB") nil)
  (define-key flyspell-mode-map (kbd "C-;") 'flyspell-auto-correct-previous-word)
  (define-key flyspell-mode-map (kbd "C-,") nil))

;; === DEFINE-WORD (dictionary [at point])
;; Source: https://github.com/abo-abo/define-word
;; Blog post: https://oremacs.com/2015/05/22/define-word/
;; (use-package define-word
;;   :bind
;;   ("C-c d" . define-word)
;;   ("C-c D" . define-word-at-point))

;; === GOOGLE TRANSLATE
;; documentation: https://github.com/atykhonov/google-translate/tree/bf119aac424994d2aa91fce9630adc01ed0ea617
;; (use-package google-translate
;;   :defer t)

;; === TRANSLATE
;; minor mode for translate-shell https://github.com/soimort/translate-shell
;; actually, this code: https://github.com/soimort/translate-shell/blob/develop/google-translate-mode.el
;; (load "~/.emacs.d/mine/packs/google-translate-mode.el")

;; === MARKDOWN
;; https://github.com/jrblevin/markdown-mode
(use-package markdown-mode
  :defer t)

;; === CSV
(use-package csv-mode
  :defer t)

(use-package pdf-tools  ;; install separately (via package manager)
  :pin manual           ;; manually update
  :defer t
  :config
  ;; initialise
  (pdf-loader-install)
  (setq-default pdf-view-display-size 'fit-width)
  ;; turn off cua so copy works
  (add-hook 'pdf-view-mode-hook
            (lambda () (cua-mode 0)))
  ;; more fine-grained zoom
  (setq pdf-view-resize-factor 1.1)
  ;; continuous scroll in pdf-view
  (setq pdf-view-continuous t)
  (add-hook 'pdf-view-mode-hook (lambda ()
                                  (pdf-view-midnight-minor-mode)))
  ;; === PDF VIEW COLORS (TEXT . BG)
  (setq pdf-view-midnight-colors (quote ("#fffbe0" . "#222222")))
        ;; monokai        (quote ("#fcfcfa" . "#2d2a2e")))
                                        ; automatically turns on midnight-mode for pdfs
  (add-to-list 'auto-mode-alist '("\\.pdf$\\'" . pdf-view-mode)))

;; === IMAGES & PDFs
;; uses imagemagick
(eval-after-load 'image '(use-package image+ :defer t))
(eval-after-load 'image+ '(imagex-global-sticky-mode 1))
(add-hook 'image-mode-hook
          (lambda ()
            (imagex-auto-adjust-mode t)
            (imagex-global-sticky-mode)))

(use-package auctex
  :defer t      ;; MUST!!
  :ensure t)

;; use pdfview with auctex
(setq TeX-view-program-selection '((output-pdf "pdf-tools")))
(setq TeX-view-program-list '(("pdf-tools" "TeX-pdf-tools-sync-view")))
(setq TeX-source-correlate-mode t)
(setq TeX-source-correlate-start-server t)
(setq TeX-source-correlate-method (quote synctex))
(setq TeX-PDF-mode t)
(setq TeX-fold-mode 1)
;; automatically reload pdf after compilation
(add-hook 'TeX-after-compilation-finished-functions
          #'TeX-revert-document-buffer)

(add-hook 'doc-view-mode-hook
          (lambda ()
            (pdf-tools-install)))

(use-package reftex
  :defer t
  :commands turn-on-reftex
  :diminish reftex-mode
  :init
  (progn
    (setq reftex-plug-into-AUCTeX t)
    (setq reftex-plug-into-auctex t)
    (setq reftex-extra-bindings t)))
;; C-c t    reftex-toc
;; C-c l    reftex-label
;; C-c r    reftex-reference
;; C-c c    reftex-citation
;; C-c v    reftex-view-crossref
;; C-c s    reftex-search-document
;; C-c g    reftex-grep-document

;; Turn on RefTeX for AUCTeX, http://www.gnu.org/s/auctex/manual/reftex/reftex_5.html
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
;; Make RefTeX interact with AUCTeX, http://www.gnu.org/s/auctex/manual/reftex/AUCTeX_002dRefTeX-Interface.html
(setq reftex-plug-into-AUCTeX t)

(setq font-latex-fontify-script nil)
;; super-/sub-script on baseline
(setq font-latex-script-display (quote (nil)))

(defun get-bibtex-keys (file)
  (with-current-buffer (find-file-noselect file)
    (mapcar 'car (bibtex-parse-keys))))

(defun LaTeX-add-all-bibitems-from-bibtex ()
  (interactive)
  (mapc 'LaTeX-add-bibitems
        (apply 'append
               (mapcar 'get-bibtex-keys (reftex-get-bibfile-list)))))

;; === YASNIPPET
(use-package yasnippet :defer t)

(add-hook 'LaTeX-mode-hook
          (lambda ()
            (yas-minor-mode)
            (diminish 'yas-minor-mode)
            (add-to-list 'yas-snippet-dirs "~/.emacs.d/mine/snips/")
            (setq yas-indent-line 'auto)
            (yas-reload-all)))

;; === COMPANY COMPLETE
;; http://company-mode.github.io/
(global-unset-key (kbd "C-3")) ;; numeric prefix, will be used for Company
(use-package company
  :diminish company-mode
  :init
  (setq company-selection-wrap-arround t
        company-show-numbers t
        company-tooltip-flip-when-above t
        company-auto-complete nil
        company-idle-delay nil)
    :hook ((merlin-mode . company-mode)
           (coq-mode . company-coq-mode))
    :bind (("C-c c o" . company-mode)   ;; global keybindings
           ("C-3" . company-complete)
           (:map company-active-map
                 ("C-n" . company-select-next)
                 ("C-p" . company-select-previous))))
;; Use numbers 0-9 (in addition to M-<num>) to select company completion candidates
;; (mapc (lambda (x)
;;         (define-key company-active-map (format "%d" x) 'company-complete-number))
;;       (number-sequence 0 9)))

;; === OCAML
;; ## added by OPAM user-setup for emacs / base ## 56ab50dc8996d2bb95e7856a6eddb17b ## you can edit, but keep this line
(require 'opam-user-setup "~/.emacs.d/opam-user-setup.el")
;; ## end of OPAM user-setup addition for emacs / base ## keep this line
(let ((opam-share (ignore-errors (car (process-lines "opam" "config" "var" "share")))))
  (when (and opam-share (file-directory-p opam-share))
    (add-to-list 'load-path (expand-file-name "emacs/site-lisp" opam-share))
    (autoload 'merlin-mode "merlin" nil t nil)
    (add-hook 'tuareg-mode-hook 'merlin-mode t)
    (add-hook 'caml-mode-hook 'merlin-mode t)))
;; Make company aware of merlin
;; (with-eval-after-load 'company
;;   (add-to-list 'company-backends 'merlin-company-backend))
                                        ; Enable company on merlin managed buffers
                                        ; Or enable it globally:
;; mostly OCAML requirements
(use-package tuareg
  :defer t)
(use-package fill-column-indicator
  :defer t)
(use-package redo+
  :defer t
  :load-path "~/.emacs.d/mine/packs/redo+"
  :bind ("C-?" . 'redo))

;; === FRAMA-C
;; (use-package acsl
;;   :defer t
;;   :load-path "~/.emacs.d/mine/packs/frama-c"
;;   :config
;;   (load-library "frama-c-recommended")
;;   (setq load-path (cons "~/.emacs.d/mine/packs/frama-c/acsl.el" load-path))
;;   (autoload 'acsl-mode "acsl"
;;   "Major mode for editing ACSL code" t)
;;   (diminish 'global-whitespace-mode)) ;; WS indicator for trailing whitespace (acsl introduces it)

;; === DAFNY, Z3 et al.
;; documentation: https://github.com/boogie-org/boogie-friends
;; (use-package boogie-friends
;;   :defer t
;;   :config
;;   (setq flycheck-dafny-executable "/usr/bin/dafny")
;;   (setq flycheck-boogie-executable "/usr/local/bin/boogie")
;;   (setq flycheck-z3-smt2-executable "/usr/bin/z3"))

;; === PYTHON
(use-package elpy
  :defer t)
;; :config
;; (elpy-enable)
;; (add-hook 'python-mode-hook 'elpy-mode)

;; === PROOF GENERAL & COQ
(use-package proof-general
  :defer t)

;; === RACKET
(use-package racket-mode
  :defer t)
;; Automatically load Racket Mode for .rkt files
(add-to-list 'auto-mode-alist '("\\.rkt$\\'" . racket-mode))

(use-package geiser
  :defer t)

;; === ISABELLE (just syntax highlight)
;; (add-to-list 'load-path "~/.emacs.d/mine/packs/simp-isar-mode/")
;; (require 'isar-mode)

(use-package company-coq
  :defer t)

;; === AGDA
;; (when window-system
;;   (load-file (let ((coding-system-for-read 'utf-8))
;;                (shell-command-to-string "agda-mode locate"))))
;; From Stump - Verified Functional Programming
; This defines backslash commands for some extra symbols.
;; (eval-after-load "quail/latin-ltx"
;;   '(mapc (lambda (pair)
;;            (quail-defrule (car pair) (cadr pair) "TeX"))
;;          '( ("\\BB" "𝔹") ("\\bl" "𝕃") ("\\bs" "𝕊") ("\\bt" "𝕋")
;;             ("\\bv" "𝕍") ("\\cv" "⋎") ("\\comp" "○") ("\\m" "↦") ("\\om" "ω"))))
;; view them all with M-x agda-input-show-translations

; This sets the Control-c Control-k shortcut to
; describe the character under your cursor.
(global-set-key "\C-c\C-k" 'describe-char)
; Change Control-c Control-, and Control-c Control-.
; in Agda mode so they show the normalized rather
; than the "simplified" goals
(defun agda2-normalized-goal-and-context ()
  (interactive)
  (agda2-goal-and-context '(3)))
(defun agda2-normalized-goal-and-context-and-inferred ()
  (interactive)
  (agda2-goal-and-context-and-inferred '(3)))
(eval-after-load "agda2-mode"
  '(progn
     (define-key agda2-mode-map (kbd "C-c C-,")
       'agda2-normalized-goal-and-context)
     (define-key agda2-mode-map (kbd "C-c C-.")
       'agda2-normalized-goal-and-context-and-inferred)))
; This sets the Agda include path.
; Change YOUR-PATH to the path on your computer
; to the Iowa Agda Library directory. Use forward
; slashes to separate subdirectories.
;; LITERATE AGDA => LATEX
;; https://agda.readthedocs.io/en/latest/tools/generating-latex.html

;; === F*
(use-package fstar-mode
  :defer t)

;; SYNTAX CHECKING
(use-package flycheck
  :ensure t
  :diminish flycheck-mode
  :defer t)

;; === HASKELL
(use-package flycheck-haskell
  :defer t)
(use-package haskell-mode
  :defer t
  :ensure company-ghc
  :ensure company-ghci
  :config
  (custom-set-variables
   '(haskell-process-suggest-remove-import-lines t)
   '(haskell-process-auto-import-loaded-modules t)
   '(haskell-process-log t)
   '(haskell-process-type 'ghci))
  (define-key haskell-mode-map (kbd "C-c C-l") 'haskell-process-load-or-reload)
  (define-key haskell-mode-map (kbd "C-`") 'haskell-interactive-bring)
  (define-key haskell-mode-map (kbd "C-c C-t") 'haskell-process-do-type)
  (define-key haskell-mode-map (kbd "C-c C-i") 'haskell-process-do-info)
  (define-key haskell-mode-map (kbd "C-c C-c") 'haskell-process-cabal-build)
  (define-key haskell-mode-map (kbd "C-c C-k") 'haskell-interactive-mode-clear)
  (define-key haskell-mode-map (kbd "C-c c") 'haskell-process-cabal)
  (add-hook 'haskell-mode-hook 'company-mode)
  (add-hook 'haskell-mode-hook 'interactive-haskell-mode)
  (add-hook 'haskell-mode-hook 'flycheck-mode)
  (add-to-list 'company-backends 'company-ghc)
  (custom-set-variables '(company-ghc-show-info t)))
(eval-after-load "haskell-mode"
  '(define-key haskell-mode-map (kbd "C-c C-c") 'haskell-compile))
(eval-after-load "haskell-cabal"
  '(define-key haskell-cabal-mode-map (kbd "C-c C-c") 'haskell-compile))
;; details https://github.com/serras/emacs-haskell-tutorial/blob/master/tutorial.md
;;;;;;;;; DEFAULT KEY BINDINGS FOR HASKELL MODE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; <f8>                                     jump to imports
;; C-C C-,                                  sort and align imports
;; M-.                                      jump to definition
;; M-x haskell-mode-stylish-buffer          format file
;; C-c C-l                                  load file in interpreter
;; C-c C-z                                  navigate errors
;; C-c C-n C-t                              show type of expression
;; C-c C-n C-i                              show info of expression
;; C-c C-n C-c                              run cabal build
;; C-c C-n c                                run cabal command
;; M-x haskell-debug                        start debugger
;; M-n                                      go to next error/warning
;; M-p                                      go to previous error/warning
;; M-?                                      show error/warning info
;; C-c C-c                                  change between GHC and Hint
;; C-c C-t                                  show type of expression (ghc-mod)
;; C-c C-i                                  show info of expression
;; M-C-d                                    show documentation of expression
;; C-c C-h                                  Hoogle search
;; M-C-i                                    auto-complete
;; M-C-m                                    insert module import
;; C-u M-t                                  initial code generation
;; M-t                                      case split
;; C-c <                                    indent region out
;; C-c >                                    indent region in
;; C-j                                      newline and indent
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LOTS OF DOCUMENTATION
;; http://haskell.github.io/haskell-mode/manual/latest/

;; === LISP (SBCL w/ SLIME)
;; (require 'slime-autoloads)
(use-package slime
  :defer t
  :config
  (setq inferior-lisp-program "/usr/bin/sbcl" ; Steel Bank Common Lisp
        slime-contribs '(slime-fancy)))

(use-package slime-company
  :ensure slime
  :ensure company
  :defer t)

;; === PHP MODE
(use-package php-mode
  :ensure t
  :defer t)

;; === WEB MODE
(use-package web-mode
  :defer t
  :config
  (add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.php?\\'" . web-mode)))

;; === ELFEED
;; more tips and tricks: https://nullprogram.com/blog/2013/11/26/
;; extra-features: https://nullprogram.com/blog/2015/12/03/
;; "s" for search filter
(use-package elfeed
  :defer t
  :config
  (setq elfeed-db-directory "~/.emacs.d/elfeed-db")
  ;; put feeds separately
  (load "~/.emacs.d/feeds.el")
  ;; play yt videos with mpv
  ;; taken from here https://www.reddit.com/r/emacs/comments/7usz5q/youtube_subscriptions_using_elfeed_mpv_no_browser/
  (defun elfeed-play-with-mpv ()
    "Play entry link with mpv."
    (interactive)
    (let ((entry (if (eq major-mode 'elfeed-show-mode) elfeed-show-entry (elfeed-search-selected :single)))
          (quality-arg "")
          (quality-val (completing-read "Max height resolution (0 for unlimited): " '("0" "480" "720") nil nil)))
      (setq quality-val (string-to-number quality-val))
      (message "Opening %s with height≤%s with mpv..." (elfeed-entry-link entry) quality-val)
      (when (< 0 quality-val)
        (setq quality-arg (format "--ytdl-format=[height<=?%s]" quality-val)))
      (start-process "elfeed-mpv" nil "mpv" quality-arg (elfeed-entry-link entry))))
  (defvar elfeed-mpv-patterns
    '("youtu\\.?be")
    "List of regexp to match against elfeed entry link to know
    whether to use mpv to visit the link.")
  (defun elfeed-visit-or-play-with-mpv ()
    "Play in mpv if entry link matches `elfeed-mpv-patterns', visit otherwise.
    See `elfeed-play-with-mpv'."
    (interactive)
    (let ((entry (if (eq major-mode 'elfeed-show-mode) elfeed-show-entry (elfeed-search-selected :single)))
          (patterns elfeed-mpv-patterns))
      (while (and patterns (not (string-match (car elfeed-mpv-patterns) (elfeed-entry-link entry))))
        (setq patterns (cdr patterns)))
      (if patterns
          (elfeed-play-with-mpv)
        (if (eq major-mode 'elfeed-search-mode)
            (elfeed-search-browse-url)
          (elfeed-show-visit)))))
  (define-key elfeed-search-mode-map (kbd "v")
    (lambda ()
      (interactive)
      (elfeed-play-with-mpv)))
  (define-key elfeed-show-mode-map (kbd "v")
    (lambda ()
      (interactive)
      (elfeed-play-with-mpv)))
  (defun elfeed-mark-all-as-read ()
    "Mark currently shown articles read"
    (interactive)
    (mark-whole-buffer)
    (elfeed-search-untag-all-unread))
  (define-key elfeed-search-mode-map (kbd "A") 'elfeed-mark-all-as-read)
  (define-key elfeed-search-mode-map (kbd "R") 'elfeed-search-fetch) ;; refresh
  (setq elfeed-search-title-max-width '100)
  (setq elfeed-search-title-trailing-width '30)       ;; space left for tags & titles
  (setq elfeed-search-title-min-width '10)
  (setq elfeed-search-date-format '("%d %b" 6 :left)) ;; 25 Mar
  (setq elfeed-search-filter "@3-days-ago +unread")   ;; default filter at startup
  (defun elfeed-open-in-ff ()
    "Open link in Firefox rather than eww"
    (interactive)
    (let ((browse-url-generic-program "firefox"))
      (elfeed-show-visit t)))
  (define-key elfeed-show-mode-map (kbd "f") 'elfeed-open-in-ff)
  (defun elfeed-open-in-next ()
    "Open link in Next browser"
    (interactive)
    (let ((browse-url-generic-program "next"))
      (elfeed-show-visit t)))
  (define-key elfeed-show-mode-map (kbd "x") 'elfeed-open-in-next)
  (define-key elfeed-search-mode-map (kbd "t") 'elfeed-search-set-feed-title))
;; DEFAULT SEARCH-MODE KEYBINDINGS
;; (define-key map "q" 'elfeed-search-quit-window)
;; (define-key map "g" 'elfeed-search-update--force)
;; (define-key map "G" 'elfeed-search-fetch)
;; (define-key map (kbd "RET") 'elfeed-search-show-entry)
;; (define-key map "s" 'elfeed-search-live-filter)
;; (define-key map "S" 'elfeed-search-set-filter)
;; (define-key map "b" 'elfeed-search-browse-url)
;; (define-key map "y" 'elfeed-search-yank)
;; (define-key map "u" 'elfeed-search-tag-all-unread)
;; (define-key map "r" 'elfeed-search-untag-all-unread)
;; (define-key map "n" 'next-line)
;; (define-key map "p" 'previous-line)
;; (define-key map "+" 'elfeed-search-tag-all)
;; (define-key map "-" 'elfeed-search-untag-all)

(use-package eww
  :if window-system
  :init
  (setq browse-url-browser-function 'eww-browse-url))
;; eww as default browser -- "&" to open in $BROWSER
;; === EWW KEYBINDINGS ===
;; g = eww-reload
;; w = eww-copy-page-url
;; M-RET = eww-open-in-new-buffer
;; R = eww-readable = extract text
;; F = eww-toggle-fonts (whether to use variable pitch fonts or not)
;; M-C = eww-toggle-colors (whether to use HTML-specified colors or not)
;; d = eww-download = download the URL under the point
;; l = eww-back-url
;; r = eww-forward-url
;; H = eww-list-histories
;; b = eww-add-bookmark
;; B = eww-list-bookmarks
;; S = eww-list-buffers (view all eww buffers open)
;; s = eww-switch-to-buffer (via a minibuffer prompt)



(use-package elfeed-web
  ;; web interface for elfeed GENIUS!!
  :defer t)

;; === MUSIC -- EMMS + MPD
;; see tutorial: https://www.youtube.com/watch?v=xTVN8UDScqk
;; his mpd.conf: https://pastebin.com/P7rwMFLq
;; his emacs.config: https://github.com/daedreth/UncleDavesEmacs#emms-with-mpd
;; more details: https://www.reddit.com/r/emacs/comments/8b0dqx/emms_updates_mpv_backend_opus_orgstyle_bindings/
;; (use-package emms
;;   :defer t
;;   :config
;;   ;; (load "~/.emacs.d/mine/packs/emms-cover.el")
;;   (require 'emms-setup)
;;   (require 'emms-cache)
;;   (require 'emms-volume)
;;   (require 'emms-volume-amixer)
;;   (require 'emms-source-file)
;;   (require 'emms-playlist-mode)
;;   (require 'emms-source-playlist)
;;   ;; (require 'emms-lyrics)
;;   (require 'emms-browser)
;;   (require 'emms-mode-line)
;;   (require 'emms-mode-line-icon)
;;   (require 'emms-playing-time)
;;   ;; (require 'emms-cover)
;;   ;; (require 'emms-player-mpv)
;;   (emms-all)
;;   (emms-standard)
;;   (emms-history-load)
;;   ;; (setq emms-player-list (list emms-player-mpv)
;;   ;;       emms-source-file-default-directory (expand-file-name "/mnt/bsg1t/music/")
;;   ;;       emms-source-file-directory-tree-function 'emms-source-file-directory-tree-find
;;   ;;       emms-browser-covers 'emms-browser-cache-thumbnail)
;;   ;; (add-to-list 'emms-player-mpv-parameters "--no-audio-display")
;;   ;; (add-to-list 'emms-info-functions 'emms-info-cueinfo)
;;   ;; (if (executable-find "emms-print-metadata")
;;   ;;     (progn
;;   ;;       (require 'emms-info-libtag)
;;   ;;       (add-to-list 'emms-info-functions 'emms-info-libtag)
;;   ;;       (delete 'emms-info-ogginfo emms-info-functions)
;;   ;;       (delete 'emms-info-mp3info emms-info-functions)
;;   ;;       )
;;   ;;   (add-to-list 'emms-info-functions 'emms-info-ogginfo)
;;   ;;   (add-to-list 'emms-info-functions 'emms-info-mp3info)
;;   ;;   )
;;   (defun z-emms-play-on-add (old-pos)
;;     "Play tracks when calling `emms-browser-add-tracks' if nothing
;;                    is currently playing."
;;     (interactive)
;;     (when (or (not emms-player-playing-p)
;;               emms-player-paused-p
;;               emms-player-stopped-p)
;;       (with-current-emms-playlist
;;         (goto-char old-pos)
;;         ;; if we're sitting on a group name, move forward
;;         (unless (emms-playlist-track-at (point))
;;           (emms-playlist-next)
;;           )
;;         (emms-playlist-select (point))
;;         )
;;       (emms-stop)
;;       (emms-start))
;;     )
;;   (add-hook 'emms-browser-tracks-added-hook 'z-emms-play-on-add)
;;   ;; Show the current track each time EMMS
;;   (add-hook 'emms-player-started-hook 'emms-show)
;;   (setq emms-show-format "Playing: %s")
;;   ;; Icon setup.
;;   (setq emms-mode-line-icon-before-format "["
;;         emms-mode-line-format " %s"
;;         emms-playing-time-display-format "%s]"
;;         emms-mode-line-icon-color "white")
;;   (setq global-mode-string '("" emms-mode-line-string " " emms-playing-time-string))
;;   ;; (defun emms-mode-line-icon-function ()
;;   ;;   (concat " "
;;   ;;           emms-mode-line-icon-before-format
;;   ;;           (propertize "NP:" 'display emms-mode-line-icon-image-cache)
;;   ;;           (format emms-mode-line-format (emms-track-get
;;   ;;                                          (emms-playlist-current-selected-track)
;;   ;;                                          'info-title))
;;   ;;           )
;;   ;;   )
;;   ;; (emms-lyrics 1)
;;   ;; (setq emms-lyrics-display-on-modeline t)
;;   (setq emms-playlist-buffer-name "*Music*")
;;   (setq emms-seek-seconds 5)
;;   (setq emms-player-list '(emms-player-mpd))
;;   (setq emms-info-functions '(emms-info-mpd))
;;   (setq emms-player-mpd-server-name "localhost")
;;   (setq emms-player-mpd-server-port "6601")
;;   ;; (setq emms-lyrics-dir "~/.emacs.d/emms/lyrics")
;;   ;; (setq emms-browser-covers 'emms-browser-cache-thumbnail)
;;   (setq emms-source-file-default-directory "/mnt/bsg1t/music"))


;; ;; === MPC
;; (setq mpc-host "localost:6601")
;; (defun mpd/start-music-daemon ()
;;   "Start MPD, connects to it and syncs the metadata cache."
;;   (interactive)
;;   (shell-command "mpd")
;;   (mpd/update-database)
;;   (emms-player-mpd-connect)
;;   (emms-cache-set-from-mpd-all)
;;   (message "MPD Started!"))

;; (defun mpd/kill-music-daemon ()
;;   "Stops playback and kill the music daemon."
;;   (interactive)
;;   (emms-stop)
;;   (call-process "killall" nil nil nil "mpd")
;;   (message "MPD Killed!"))

;; (defun mpd/update-database ()
;;   "Updates the MPD database synchronously."
;;   (interactive)
;;   (call-process "mpc" nil nil nil "update")
;;   (message "MPD Database Updated!"))

;; ;; === RADIO STATIONS
;; (defun play-smooth-jazz ()
;;   "Start up some nice Jazz"
;;   (interactive)
;;   (emms-play-streamlist "http://thejazzgroove.com/itunes.pls"))

;; (defun play-tananana ()
;;   "Start playing TANANANA"
;;   (interactive)
;;   (emms-play-streamlist "http://live.tananana.ro:8010/stream-48.aac"))

;; (defun play-europa ()
;;   "Start playing Europa FM"
;;   (interactive)
;;   (emms-play-streamlist "http://astreaming.europafm.ro:8000/europafm_aacp48k"))

;; (defun play-digi ()
;;   "Start playing Digi FM"
;;   (interactive)
;;   (emms-play-streamlist "http://edge76.rdsnet.ro:84/digifm/digifm.mp3"))

;; (defun play-rro ()
;;   "Start playing Radio Romania"
;;   (interactive)
;;   (emms-play-streamlist "http://89.238.227.6:8006/"))

;; (defun play-rockfm ()
;;   "Start playing Rock FM"
;;   (interactive)
;;   (emms-play-streamlist "http://80.86.106.143:9128/rockfm.aacp"))


;; === EMAIL (MU4E)
(use-package mu4e
  :defer t
  :if window-system
  :load-path "/usr/share/emacs/site-lisp/mu4e"
  :ensure org-mime
  :ensure htmlize
  :ensure mu4e-alert
  :init
  ;; (require 'mu4e)
  (require 'org-mu4e)
  ;; convert org messages to html
  (require 'smtpmail)
  ;; source: http://www.macs.hw.ac.uk/~rs46/posts/2014-01-13-mu4e-email-client.html
  (setq message-send-mail-function 'smtpmail-send-it
        smtpmail-starttls-credentials
        '(("smtp.fastmail.com" 587 nil nil))
        smtpmail-default-smtp-server "smtp.fastmail.com"
        smtpmail-smtp-server "smtp.fastmail.com"
        smtpmail-smtp-service 587
        smtpmail-smtp-user "adrianmanea@fastmail.fm"
        smtpmail-debug-info t
        mu4e-sent-messages-behavior 'sent
        mu4e-hide-index-messages t        ;; don't show "Indexing..."
        mu4e-index-update-in-background t
        mu4e-index-update-error-warning nil ;; don't show nonzero exit code
        mu4e-use-fancy-chars nil)           ;; show fancy (Unicode) chars for marks
  ;; lazy indexing => speedup
  (setq
   mu4e-index-cleanup nil
   mu4e-index-lazy-check t)
  (setq org-mu4e-convert-to-html t)
  (setq mu4e-maildir (expand-file-name "~/email/mbsyncmail"))
  (setq mu4e-drafts-folder "/Drafts")
  (setq mu4e-sent-folder "/Sent")
  (setq mu4e-trash-folder "/Trash")
  ;; get mail
  (setq mu4e-get-mail-command "mbsync -c ~/.emacs.d/.mbsyncrc -a"
        mu4e-html2text-command "w3m -T text/html"
        ;; comment /usr/share/emacs/site-lisp/mu4e/mu4e-message.el (require 'html2text)
        mu4e-update-interval 600 ;; in seconds
        mu4e-headers-auto-update t
        mu4e-compose-signature-auto-include nil)
  (setq mu4e-maildir-shortcuts
        '(("/INBOX" . ?i)
          ("/Sent" . ?s)
          ("/Trash" . ?t)
          ("/Drafts" . ?d)))
  ;; headers to show in mu4e
  ;; header . characters
  (setq mu4e-headers-date-format "%d %b %Y"
        mu4e-headers-time-format "%H:%M")
  (setq mu4e-headers-fields
        '((:date . 15)
          (:flags . 5)
          (:from-or-to . 30)
          (:thread-subject . nil))) ;; unlimited
  ;; don't keep message buffers around
  (setq message-kill-buffer-on-exit t)
  ;; show-images
  (setq mu4e-show-images t)
  ;; use imagemagick, if available
  (when (fboundp 'imagemagick-register-types)
    (imagemagick-register-types))
  ;; don't inherit content from emacs-mail
  (setq mu4e-reply-to-address "adrianmanea@fastmail.fm"
        user-mail-address "adrianmanea@fastmail.fm"
        user-full-name "Adrian Manea")
  (add-hook 'mu4e-compose-mode-hook
            '(lambda()
               (turn-on-auto-fill)
               (set-fill-column 80)))
  ;; Open message in browser
  (defun mu4e-msgv-action-view-in-browser (msg)
    "View the body of the message in a web browser."
    (interactive)
    (let ((html (mu4e-msg-field (mu4e-message-at-point t) :body-html))
          (tmpfile (format "%s/%d.html" temporary-file-directory (random))))
      (unless html (error "No html part for this message"))
      (with-temp-file tmpfile
        (insert
         "<html>"
         "<head><meta http-equiv=\"content-type\""
         "content=\"text/html;charset=UTF-8\">"
         html))
      (browse-url (concat "file://" tmpfile))))
  (add-hook 'mu4e-view-mode-hook
            (lambda()
              (add-to-list 'mu4e-view-actions
                           '("View in browser" . mu4e-msgv-action-view-in-browser) t)))
  ;; press "a" to open available actions
  (require 'gnus-dired)
  ;; make the `gnus-dired-mail-buffers' function also work on
  ;; message-mode derived modes, such as mu4e-compose-mode
  ;; from here https://zmalltalker.com/linux/mu.html
  (defun gnus-dired-mail-buffers ()
    "Return a list of active message buffers."
    (let (buffers)
      (save-current-buffer
        (dolist (buffer (buffer-list t))
          (set-buffer buffer)
          (when (and (derived-mode-p 'message-mode)
                     (null message-sent-message-via))
            (push (buffer-name buffer) buffers))))
      (nreverse buffers)))
  (setq gnus-dired-mail-mode 'mu4e-user-agent)
  (add-hook 'dired-mode-hook 'turn-on-gnus-dired-mode)
  ;; dired C-c RET C-a = attach file
  (add-hook 'after-init-hook #'mu4e-alert-enable-mode-line-display))


;; (use-package org-mime)
;; ;; documentation: https://github.com/hniksic/emacs-htmlize
;; (use-package htmlize)

;; === MU4E-ALERT: New message notification
;; details: https://github.com/iqbalansari/mu4e-alert
(use-package mu4e-alert
  :defer t)

;; === REDDIT --- WIP
;; (use-package md4rd
;;   :defer t)
;; (define-key map (kbd "u") 'tree-mode-goto-parent)
;;  (define-key map (kbd "o") 'md4rd-open)
;;  (define-key map (kbd "v") 'md4rd-visit)
;;  (define-key map (kbd "e") 'tree-mode-toggle-expand)
;;  (define-key map (kbd "E") 'md4rd-widget-expand-all)
;;  (define-key map (kbd "C") 'md4rd-widget-collapse-all)
;;  (define-key map (kbd "n") 'widget-forward)
;;  (define-key map (kbd "j") 'widget-forward)
;;  (define-key map (kbd "h") 'backward-button)
;;  (define-key map (kbd "p") 'widget-backward)
;;  (define-key map (kbd "k") 'widget-backward)
;;  (define-key map (kbd "l") 'forward-button)
;;  (define-key map (kbd "q") 'kill-current-buffer)
;;  (define-key map (kbd "r") 'md4rd-reply)
;;  (define-key map (kbd "u") 'md4rd-upvote)
;;  (define-key map (kbd "d") 'md4rd-downvote)
;;  (define-key map (kbd "t") 'md4rd-widget-toggle-line)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; === GLOBAL KEYBINDINGS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define-key global-map (kbd "C-c f f") 'toggle-frame-fullscreen)
(global-unset-key (kbd "C-z")) ;; minimize (CAUSES HANG depending on WM!)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; === CUSTOM FUNCTIONS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; === COPY BUFFER NAME
(defun my-put-file-name-on-clipboard ()
  "Put the current file name on the clipboard"
  (interactive)
  (let ((filename (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name))))
    (when filename
      (with-temp-buffer
        (insert filename)
        (clipboard-kill-region (point-min) (point-max)))
      (message filename))))


;; === TOGGLE TRANSPARENCY
(defun toggle-transparency ()
  (interactive)
  (let ((alpha (frame-parameter nil 'alpha)))
    (set-frame-parameter
     nil 'alpha
     (if (eql (cond ((numberp alpha) alpha)
                    ((numberp (cdr alpha)) (cdr alpha))
                    ;; Also handle undocumented (<active> <inactive>) form.
                    ((numberp (cadr alpha)) (cadr alpha)))
              100)
         '(75 . 75) '(100 . 100)))))
(global-set-key (kbd "C-c t") 'toggle-transparency)

;; ===RENAME FILE AND BUFFER
(defun rename-file-and-buffer (new-name)
  "Renames both current buffer and file it's visiting to NEW-NAME."
  (interactive "sNew name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer new-name)
          (message "A buffer named '%s' already exists!" new-name)
        (progn
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil))))))

;; === SWAP WINDOWS IN SPLIT
(defun win-swap ()
  "Swap windows using buffer-move.el" ;; it seems it's built-in
  (interactive)
  (if
      (null (windmove-find-other-window 'right))
      (buf-move-left)
    (buf-move-right)))

;; === PASTE IN TERMINAL
;; S-Insert by default
(add-hook 'term-mode-hook
          (lambda ()
            (define-key term-raw-map (kbd "C-y") 'term-paste)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; === FIX FOR CONTINUOUS PDF-VIEW SCROLL-OTHER-WINDOW
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar-local sow-scroll-up-command nil)

(defvar-local sow-scroll-down-command nil)

(defvar sow-mode-map
  (let ((km (make-sparse-keymap)))
    (define-key km [remap scroll-other-window] 'sow-scroll-other-window)
    (define-key km [remap scroll-other-window-down] 'sow-scroll-other-window-down)
    km)
  "Keymap used for `sow-mode'")

(define-minor-mode sow-mode
  "FIXME: Not documented."
  nil nil nil
  :global t)

(defun sow-scroll-other-window (&optional arg)
  (interactive "P")
  (sow--scroll-other-window-1 arg))

(defun sow-scroll-other-window-down (&optional arg)
  (interactive "P")
  (sow--scroll-other-window-1 arg t))

(defun sow--scroll-other-window-1 (n &optional down-p)
  (let* ((win (other-window-for-scrolling))
         (cmd (with-current-buffer (window-buffer win)
		(if down-p
		    (or sow-scroll-down-command #'scroll-up-command)
		  (or sow-scroll-up-command #'scroll-down-command)))))
    (with-current-buffer (window-buffer win)
      (save-excursion
        (goto-char (window-point win))
        (with-selected-window win
          (funcall cmd n))
        (set-window-point win (point))))))

(add-hook 'Info-mode-hook
	  (lambda nil
	    (setq sow-scroll-up-command
		  (lambda (_) (Info-scroll-up))
		  sow-scroll-down-command
		  (lambda (_) (Info-scroll-down)))))

(add-hook 'doc-view-mode-hook
          (lambda nil
            (setq sow-scroll-up-command
                  'doc-view-scroll-up-or-next-page
                  sow-scroll-down-command
                  'doc-view-scroll-down-or-previous-page)))

(add-hook 'pdf-view-mode-hook
          (lambda nil
            (setq sow-scroll-up-command
                  'pdf-view-scroll-up-or-next-page
                  sow-scroll-down-command
                  'pdf-view-scroll-down-or-previous-page)))

(provide 'scroll-other-window)
;;; scroll-other-window.el ends here
(global-unset-key (kbd "C-M-v")) ;; original scroll other window DOWN
(global-unset-key (kbd "M-<prior>"))
(global-unset-key (kbd "C-M-S-v")) ;; original scroll other window UP
(global-unset-key (kbd "M-<next>"))
(define-key global-map (kbd "M-<prior>") 'sow-scroll-other-window-down)
(define-key global-map (kbd "M-<next>") 'sow-scroll-other-window)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; === FONT (FACES) SETTINGS
;; === BITMAP ===
;; (set-frame-font "Misc Fixed 12")
;; (set-frame-font "Dina")
;; (set-frame-font "UW Ttyp0 13")
;; (set-frame-font "xos4 Terminus 12")
;; (set-frame-font "Fixedsys Excelsior 12")
;; (set-frame-font "DOSEGA 13")

;; === TRUETYPE ===
;; (set-frame-font "Liberation Mono 11")
;; (set-frame-font "Consolas 12")
;; (set-frame-font "Input Mono Narrow 11")
;; (set-frame-font "DejaVu Sans Mono 11")
;; (set-frame-font "Droid Sans Mono 11")
;; (set-frame-font "Anonymous Pro 12")
;; (set-frame-font "Cousine 11")
;; (set-frame-font "IBM Plex Mono 11")

;; === NARROW ===
(set-frame-font "PragmataPro Mono 12")
;; (set-frame-font "M+ 1m 12")
;; (set-frame-font "Iosevka 12")

(setq-default line-spacing 3)


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (geiser racket-mode pandoc multiple-cursors twittering-mode bug-hunter zenburn-theme wttrin web-mode use-package undo-tree tuareg solarized-theme slime rainbow-mode rainbow-delimiters proof-general php-mode org-ref org-mime nyan-mode nov nord-theme mu4e-alert monokai-theme moe-theme md4rd markdown-mode magit json-mode image+ hide-mode-line google-translate fstar-mode flycheck-haskell fill-column-indicator emms elpy elfeed-web doom-themes djvu dired-narrow diminish define-word csv-mode counsel company-ghci company-ghc company-coq boogie-friends avy auctex all-the-icons-dired))))
