scriptencoding utf-8
set encoding=utf-8

" -------- PLUGINS ---------"
set nocompatible 			" lose compatibility with simple vi
filetype off 				" required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'ctrlpvim/ctrlp.vim' 			" fuzzy finder
Plugin 'lervag/vimtex' 				    " lightweight LaTeX suite
Plugin 'godlygeek/tabular'              " for markdown
Plugin 'plasticboy/vim-markdown'        " https://github.com/plasticboy/vim-markdown
Plugin 'dhruvasagar/vim-table-mode'     " <Leader>tm
Plugin 'lilydjwg/colorizer'             " highlight color codes
Plugin 'scrooloose/nerdcommenter'       " smart comment <leader>cc <leader>cu

" COLORSCHEMES
Plugin 'jnurmine/Zenburn'
Plugin 'arcticicestudio/nord-vim'
Plugin 'morhetz/gruvbox'
Plugin 'joshdick/onedark.vim'
Plugin 'dracula/vim', { 'name': 'dracula' }
Plugin 'crusoexia/vim-monokai'

" All plugins must be added before the following line
call vundle#end() 			" required
filetype plugin indent on
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Non-Plugin Stuff
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Open file under cursor with default program
nnoremap gO :!nnn <cfile><CR>

" Move by visual lines
noremap <silent> k gk
noremap <silent> j gj
noremap <silent> 0 g0
noremap <silent> $ g$

" Hide some confirmation messages
set shortmess=a 

" Navigating with guides (using <++> placeholder)
inoremap <C-j> <esc>/<++><enter>"_c4l
vnoremap <C-j> <esc>/<++><enter>"_c4l
map <C-j> <esc>/<++><enter>"_c4l

if has('clipboard')
    set clipboard=unnamedplus   	" interact with system clipboard
    set guioptions+=a
endif
set ttimeout
set ttimeoutlen=10 				" 10ms wait for commands
set timeout					    " for mappings
set timeoutlen=1000 				" 1 second, default
set backspace=indent,eol,start 			" use backspace as normal
set scrolloff=3 				" keep 3 lines below and above the cursor
set undolevels=25 				" maximum allowed
set number relativenumber 				" relative line numbers
set incsearch					" incremental search
set guioptions-=l 				" eliminate scrollbars
set guioptions-=L
set guioptions-=r
set guioptions-=R
set splitbelow					" default h split puts below
set splitright 					" default v split puts right
set noerrorbells visualbell t_vb= 		" disable bell
set autowriteall				" autosave when switching buffers
set shiftwidth=4				" 4 spaces in autoindent
set tabstop=4					" tab in normal mode
set softtabstop=4				" tab in insert mode
set expandtab					" convert tab to spaces
set autoread					" reload files changed outside
set wrapmargin=0
set history=100					" how many lines of history it remembers
set showmatch					" highlight matching brackets
set ai						" autoindent
set si						" smartindent
set linebreak					" wrap lines at convenient points

"ignore some filetypes for completion
set wildignore+=*.aux,*.toc,*.synctex.gz,*.fdb_latexmk
set wildmenu					" visual autocomplete for cmd

" ruler with date and time
" show statusline
set laststatus=2
set ruler
set rulerformat=%33(%{strftime('%a\ %e\ %b\ %H:%M')}\ %5l,%-6(%c%)\ %P%)


" set the command window height to 2 lines
set cmdheight=2

" search
set ignorecase              " case insensitive searching
set smartcase               " case sensitive if expression contains a capital
set magic                   " set magic on, for regex

set t_Co=256
syntax on
set background=dark
colorscheme nord

" Scroll other window with M-j, M-k
nmap <a-j> <c-w>w<c-e><c-w>w
nmap <a-k> <c-w>w<c-y><c-w>w

" ================ Persistent Undo ==================
" Keep undo history across sessions, by storing in file.
" Only works all the time.
let s:vim_cache = expand('$HOME/.vim/backups')
if filewritable(s:vim_cache) == 0 && exists("*mkdir")
    call mkdir(s:vim_cache, "p", 0700)
endif

" ---------General-LaTeX-----------"
filetype indent on
set grepprg=grep\ -nH\ $*       " better search in tex source files
let g:tex_flavor='latex'        " set default type for .tex
let g:Tex_DefaultTargetFormat='pdf'
let g:tex_fast='c,m,M,p,r,s,S,v,V'
let g:tex_conceal=""            " do not hide $ and other symbols
set matchpairs+={:}

" Setup the compile rule for pdf to use pdflatex with synctex enabled
let g:Tex_CompileRule_pdf = 'pdflatex -synctex=1 --interaction=nonstopmode $*' 

" --------vimtex-----------------"
nmap <leader>xt :VimtexTocToggle<CR>
let g:vimtex_compiler_latexmk = {'callback' : 0}

" edit vimrc in a new tab
nmap <Leader>ev :tabedit ~/.vimrc<cr>

" source current file
nmap <Leader>so :source %<cr>

"---------CtrlP-------------"
let g:ctrlp_match_window = 'bottom,order:ttb'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 0

"-----------Markdown-------------"
"    zr: reduces fold level throughout the buffer
"    zR: opens all folds
"    zm: increases fold level throughout the buffer
"    zM: folds everything all the way
"    za: open a fold your cursor is on
"    zA: open a fold your cursor is on recursively
"    zc: close a fold your cursor is on
"    zC: close a fold your cursor is on recursively
let g:vim_markdown_folding_disabled = 0     " enable folding
let g:vim_markdown_folding_level = 2        " fold at subheaders
set conceallevel=2                          " vim standard conceal config
let g:vim_markdown_new_list_item_indent = 0 " don't indent further next item
"-----------BACKUP---------------"
set backup
set backupdir=~/.vim-tmp
" for swap
set directory=~/.vim-tmp

" do not autocomment
autocmd BufNewFile,BufRead * setlocal formatoptions-=ro

"--------LaTeX---------"
" Text styles
autocmd BufNewFile,BufRead *.tex inoremap ;em \emph{}<++><Esc>T{i
autocmd BufNewFile,BufRead *.tex inoremap ;bf \textbf{}<++><Esc>T{i
autocmd BufNewFile,BufRead *.tex inoremap ;it \textit{}<++><Esc>T{i
autocmd BufNewFile,BufRead *.tex inoremap ;tt \texttt{}<++><Esc>T{i
autocmd BufNewFile,BufRead *.tex inoremap ;sc \textsc{}<++><Esc>T{i
autocmd BufNewFile,BufRead *.tex inoremap ;fk \mathfrak{}<++><Esc>T{i
autocmd BufNewFile,BufRead *.tex inoremap ;bb \mathbb{}<++><Esc>T{i
autocmd BufNewFile,BufRead *.tex inoremap ;ka \mathcal{}<++><Esc>T{i

" Environments
autocmd BufNewFile,BufRead *.tex inoremap ;ol \begin{enumerate}<Enter><Enter>\end{enumerate}<Enter><Enter><++><Esc>3kA\item<Space>
autocmd BufNewFile,BufRead *.tex inoremap ;aol \begin{enumerate}[(a)]<Enter><Enter>\end{enumerate}<Enter><Enter><++><Esc>3kA\item<Space>
autocmd BufNewFile,BufRead *.tex inoremap ;1ol \begin{enumerate}[(1)]<Enter><Enter>\end{enumerate}<Enter><Enter><++><Esc>3kA\item<Space>
autocmd BufNewFile,BufRead *.tex inoremap ;ul \begin{itemize}<Enter><Enter>\end{itemize}<Enter><Enter><++><Esc>3kA\item<Space>
autocmd BufNewFile,BufRead *.tex inoremap ;li \item<Space>
autocmd BufNewFile,BufRead *.tex inoremap ;tab \begin{tabular}<Enter><++><Enter>\end{tabular}<Enter><Enter><++><Esc>4kA{}<Esc>i
autocmd BufNewFile,BufRead *.tex inoremap ;sec \section{}<Enter><Enter><++><Esc>2kf}i
autocmd BufNewFile,BufRead *.tex inoremap ;ssec \subsection{}<Enter><Enter><++><Esc>2kf}i
autocmd BufNewFile,BufRead *.tex inoremap ;sssec \subsubsection{}<Enter><Enter><++><Esc>2kf}i
autocmd BufNewFile,BufRead *.tex inoremap ;beg \begin{%DELRN%}<Enter><++><Enter>\end{%DELRN%}<Enter><Enter><++><Esc>4kfR:MultipleCursorsFind<Space>%DELRN%<Enter>c
autocmd BufNewFile,BufRead *.tex inoremap ;up <Esc>/usepackage<Enter>o\usepackage{}<Esc>i
autocmd BufNewFile,BufRead *.tex nnoremap ;up /usepackage<Enter>o\usepackage{}<Esc>i
autocmd BufNewFile,BufRead *.tex inoremap ;al* \begin{align*}<Enter><Space><Space><Enter>\end{align*}<Enter><Enter><++><Esc>3kA

"""" Math Environments
autocmd BufNewFile,BufRead *.tex inoremap ;thm \begin{theorem}\label{thm:}<Enter><++><Enter>\end{theorem}<Enter><Enter><++><Esc>4k$i
autocmd BufNewFile,BufRead *.tex inoremap ;nth \begin{theorem}[]\label{thm:<++>}<Enter><++><Enter>\end{theorem}<Enter><Enter><++><Esc>4kf[a
autocmd BufNewFile,BufRead *.tex inoremap ;pp \begin{proposition}\label{prop:}<Enter><++><Enter>\end{proposition}<Enter><Enter><++><Esc>4k$i
autocmd BufNewFile,BufRead *.tex inoremap ;npp \begin{proposition}[]\label{prop:}<Enter><++><Enter>\end{proposition}<Enter><Enter><++><Esc>4kf[a
autocmd BufNewFile,BufRead *.tex inoremap ;le \begin{lemma}\label{le:}<Enter><++><Enter>\end{lemma}<Enter><Enter><++><Esc>4k$i
autocmd BufNewFile,BufRead *.tex inoremap ;nle \begin{lemma}[]\label{le:<++>}<Enter><++><Enter>\end{lemma}<Enter><Enter><++><Esc>4kf[a
autocmd BufNewFile,BufRead *.tex inoremap ;co \begin{corollary}\label{cor:}<Enter><++><Enter>\end{corollary}<Enter><Enter><++><Esc>4k$i
autocmd BufNewFile,BufRead *.tex inoremap ;nco \begin{corollary}[]\label{cor:}<Enter><++><Enter>\end{corollary}<Enter><Enter><++><Esc>4kf[a
autocmd BufNewFile,BufRead *.tex inoremap ;pf \begin{proof}<Enter><Enter>\end{proof}<Enter><Enter><++><Esc>3k$i
autocmd BufNewFile,BufRead *.tex inoremap ;def \begin{definition}\label{def:}<Enter><++><Enter>\end{definition}<Enter><Enter><++><Esc>4k$i
autocmd BufNewFile,BufRead *.tex inoremap ;ex \begin{example}<Enter><Enter>\end{example}<Enter><Enter><++><Esc>3k$i
autocmd BufNewFile,BufRead *.tex inoremap ;rk \begin{remark}\label{rk:}<Enter><++><Enter>\end{remark}<Enter><Enter><++><Esc>4k$i
autocmd BufNewFile,BufRead *.tex inoremap ;cm \[<Enter><Space><Space><Enter>\]<Enter><++><Esc>2k$a
autocmd BufNewFile,BufRead *.tex inoremap ;ca \begin{cases}<Enter><Space><Space><Space>&<Space><++>\\<Enter><++><Enter>\end{cases}<Enter><++><Esc>3ki




""" Symbols and Commands 
autocmd BufNewFile,BufRead *.tex inoremap ;ref \ref{}<Space><++><Esc>T{i
autocmd BufNewFile,BufRead *.tex inoremap ;ra \rightarrow
autocmd BufNewFile,BufRead *.tex inoremap ;la \leftarrow
autocmd BufNewFile,BufRead *.tex inoremap ;lra \leftrightarrow
autocmd BufNewFile,BufRead *.tex inoremap ;fa \forall
autocmd BufNewFile,BufRead *.tex inoremap ;ex \exists
autocmd BufNewFile,BufRead *.tex inoremap ;Ra \Rightarrow
autocmd BufNewFile,BufRead *.tex inoremap ;La \Leftarrow
autocmd BufNewFile,BufRead *.tex inoremap ;Lra \Leftrightarrow
autocmd BufNewFile,BufRead *.tex inoremap ;LRa \Longrightarrow
autocmd BufNewFile,BufRead *.tex inoremap ;LLR \Longleftrightarrow
autocmd BufNewFile,BufRead *.tex inoremap ;pd \frac{\partial }{\partial <++>}<Space><++><Esc>2F}i
autocmd BufNewFile,BufRead *.tex inoremap ;00 \infty
autocmd BufNewFile,BufRead *.tex inoremap ;\ \textbackslash
autocmd BufNewFile,BufRead *.tex inoremap ;iii \int_{-\infty}^{\infty} 
autocmd BufNewFile,BufRead *.tex inoremap ;ioi \int_0^\infty 




""" Greek Letters
autocmd BufNewFile,BufRead *.tex inoremap ,a \alpha
autocmd BufNewFile,BufRead *.tex inoremap ,b \beta
autocmd BufNewFile,BufRead *.tex inoremap ,e \varepsilon
autocmd BufNewFile,BufRead *.tex inoremap ,f \varphi
autocmd BufNewFile,BufRead *.tex inoremap ,l \lambda
autocmd BufNewFile,BufRead *.tex inoremap ,L \Lambda
autocmd BufNewFile,BufRead *.tex inoremap ,g \gamma
autocmd BufNewFile,BufRead *.tex inoremap ,G \Gamma
autocmd BufNewFile,BufRead *.tex inoremap ,d \delta
autocmd BufNewFile,BufRead *.tex inoremap ,D \Delta
autocmd BufNewFile,BufRead *.tex inoremap ,t \theta
autocmd BufNewFile,BufRead *.tex inoremap ,T \Theta
autocmd BufNewFile,BufRead *.tex inoremap ,o \omega
autocmd BufNewFile,BufRead *.tex inoremap ,O \Omega

" forceful fix for thehighlight in emph
"autocmd BufNewFile,BufRead *.tex :colorscheme my-simple

" no soft wrap in latex
"autocmd BufNewFile,BufRead *.tex :set columns=<CR>




""" Most common misspellings
iab đfrac dfrac
iab đots dots
iab ßqrt sqrt
iab ƒrac frac
iab ƒorall forall
