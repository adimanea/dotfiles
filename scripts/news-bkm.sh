#!/bin/sh

[ "$#" -eq 0 ] && exit 1

url="$1"
title="$2"
description="$3"
feed_title="$4"

grep -q "${feed_title}\n${title}\t${url}\n${description}" ~/notes/news-bookmarks.txt || echo -e "${feed_title}\n${title}\t${url}\n${description}" >> ~/notes/news-bookmarks.txt

