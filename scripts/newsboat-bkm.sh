#!/bin/sh
# this is a simple example script that demonstrates how bookmarking plugins for newsbeuter are implemented
# (c) 2007 Andreas Krennmair
# (c) 2016 Alexander Batischev

url="$1"
title="$2"
description="$3"
feed_title="$4"

echo -e "## ${feed_title}\n- [${title}](${url}):${description}" >> ~/Documents/saved-articles/newsboat-bookmarks.md
#echo -e "* ${feed_title}\n[[${url}][${title}]] -- ${description}" >> ~/Documents/saved-articles/newsboat-bookmarks.org
#echo -e "#${feed_title}\n[${title}](${url})\t${description}" >> ~/bookmarks.md
#echo -e "${url}\t${title}\t${description}\t${feed_title}" >> ~/bookmarks.txt
